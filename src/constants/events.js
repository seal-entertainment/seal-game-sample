export const EVENT_NAME = {
  LOGIN: "LoginAndGetPlayerInfo",
  ON_LOGIN: "OnLoginAndGetPlayerInfo",
  GET_RULE: "GetGameRule",
  ON_GET_RULE: "OnGetGameRule",
  GET_BALANCE: "GetBalance",
  ON_GET_BALANCE: "OnGetBalance",
  PLACE_BET: "PlaceBet",
  ON_PLACE_BET: "OnPlaceBet",
  BUY_FREE_GAME: "BuyFreeGame",
  ON_BUY_FREE_GAME: "OnBuyFreeGame",

  CONNECTION_SUCCESS: "ConnectionSuccess",
  SHOW_ERROR: "ShowErrorMessage",
  LOADING_PROGRESS: "LoadingProgress",
  DOUBLE_COMPLETED: "DoubleAnimationCompleted",
  CARDS_READY: "CardsReady",
  ANIMATION_COMPLETED: "AnimationCompleted",
  CAN_NEXT: "CanNext",
  TURBO_CHANGED: "TurboChanged",
  UPDATE_VALUE: "UpdateValue",
  UPDATE_BET: "UpdateBet",
  AUTOPLAY: "Autoplay",
  STOP_AUTO: "StopAuto",
  CRASH: "Crash",
  CLOSE_POPUP: "ClosePopup",
  CLOSE_FREE_GAME_POPUP: "CloseFreeGamePopup",
  ENTER_FREE: "EnterFree",
  DOUBLE_CHANCE:"DoubleChance"
};

export const CONNECTION_EVENT = "ConnectionEvent";
