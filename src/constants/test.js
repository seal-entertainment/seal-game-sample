export const hitDoubleNoWin = {
  Double: [25],
  Cards: [
    {
      Columns: [
        {
          Symbol: [8, 6, 5, 8, 6],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 4, 7, 2, 7],
          RemoveAmount: 3,
        },
        {
          Symbol: [7, 2, 3, 1, 7],
          RemoveAmount: 2,
        },
        {
          Symbol: [7, 4, 6, 6, 4],
          RemoveAmount: 1,
        },
        {
          Symbol: [4, 5, 6, 925, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [5, 7, 7, 2, 2],
          RemoveAmount: 2,
        },
      ],
      HitSymbol: [7],
      Payoff: [0.3],
    },
    {
      Columns: [
        {
          Symbol: [8, 6, 5, 8, 6],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 8, 8, 4, 2],
          RemoveAmount: 0,
        },
        {
          Symbol: [3, 4, 2, 3, 1],
          RemoveAmount: 0,
        },
        {
          Symbol: [4, 4, 6, 6, 4],
          RemoveAmount: 0,
        },
        {
          Symbol: [4, 5, 6, 925, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 6, 5, 2, 2],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [],
      Payoff: [],
    },
  ],
  PromoActivity: {
    FreeRound: "0",
  },
  CustName: "twpgmsobichen",
  Balance: 100550.54,
  WinAmount: 7.5,
  FreeRound: 0,
};

export const hitDoubleAndWin = {
  Double: [25],
  Cards: [
    {
      Columns: [
        {
          Symbol: [4, 8, 6, 4, 7],
          RemoveAmount: 1,
        },
        {
          Symbol: [4, 6, 4, 8, 7],
          RemoveAmount: 1,
        },
        {
          Symbol: [7, 7, 7, 7, 4],
          RemoveAmount: 4,
        },
        {
          Symbol: [6, 8, 1, 925, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 4, 5, 4, 5],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 7, 0, 1, 7],
          RemoveAmount: 2,
        },
      ],
      HitSymbol: [7],
      HitAmount: [8],
      Payoff: [0.3],
    },
    {
      Columns: [
        {
          Symbol: [5, 4, 8, 6, 4],
          RemoveAmount: 2,
        },
        {
          Symbol: [3, 4, 6, 4, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [7, 7, 4, 5, 4],
          RemoveAmount: 2,
        },
        {
          Symbol: [6, 8, 1, 925, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 4, 5, 4, 5],
          RemoveAmount: 2,
        },
        {
          Symbol: [8, 7, 6, 0, 1],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [4],
      HitAmount: [8],
      Payoff: [0.6],
    },
    {
      Columns: [
        {
          Symbol: [7, 5, 5, 8, 6],
          RemoveAmount: 1,
        },
        {
          Symbol: [6, 8, 3, 6, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [6, 2, 7, 7, 5],
          RemoveAmount: 1,
        },
        {
          Symbol: [6, 8, 1, 925, 8],
          RemoveAmount: 1,
        },
        {
          Symbol: [6, 8, 6, 5, 5],
          RemoveAmount: 2,
        },
        {
          Symbol: [8, 7, 6, 0, 1],
          RemoveAmount: 1,
        },
      ],
      HitSymbol: [6],
      HitAmount: [8],
      Payoff: [0.4],
    },
    {
      Columns: [
        {
          Symbol: [8, 7, 5, 5, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [4, 6, 8, 3, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [7, 2, 7, 7, 5],
          RemoveAmount: 0,
        },
        {
          Symbol: [1, 8, 1, 925, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [4, 0, 8, 5, 5],
          RemoveAmount: 1,
        },
        {
          Symbol: [5, 8, 7, 0, 1],
          RemoveAmount: 1,
        },
      ],
      HitSymbol: [8],
      HitAmount: [8],
      Payoff: [0.2],
    },
    {
      Columns: [
        {
          Symbol: [5, 5, 7, 5, 5],
          RemoveAmount: 4,
        },
        {
          Symbol: [6, 7, 4, 6, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 2, 7, 7, 5],
          RemoveAmount: 1,
        },
        {
          Symbol: [0, 8, 1, 1, 925],
          RemoveAmount: 0,
        },
        {
          Symbol: [8, 4, 0, 5, 5],
          RemoveAmount: 2,
        },
        {
          Symbol: [5, 5, 7, 0, 1],
          RemoveAmount: 2,
        },
      ],
      HitSymbol: [5],
      HitAmount: [9],
      Payoff: [0.5],
    },
    {
      Columns: [
        {
          Symbol: [5, 5, 2, 8, 7],
          RemoveAmount: 1,
        },
        {
          Symbol: [6, 7, 4, 6, 3],
          RemoveAmount: 1,
        },
        {
          Symbol: [4, 7, 2, 7, 7],
          RemoveAmount: 3,
        },
        {
          Symbol: [0, 8, 1, 1, 925],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 7, 8, 4, 0],
          RemoveAmount: 2,
        },
        {
          Symbol: [6, 6, 7, 0, 1],
          RemoveAmount: 1,
        },
      ],
      HitSymbol: [7],
      HitAmount: [8],
      Payoff: [0.3],
    },
    {
      Columns: [
        {
          Symbol: [7, 5, 5, 2, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [2, 6, 4, 6, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 2, 3, 4, 2],
          RemoveAmount: 0,
        },
        {
          Symbol: [0, 8, 1, 1, 925],
          RemoveAmount: 0,
        },
        {
          Symbol: [8, 6, 8, 4, 0],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 6, 6, 0, 1],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [],
      Payoff: [],
    },
  ],
  PromoActivity: {
    FreeRound: "0",
  },
  FreeGame: [],
  CustName: "twpgmsobichen",
  Balance: 100415.64,
  TotalPayoff: 57.5,
  Scatter: { Amount: 0, FreeTimes: 0 },
};

export const hitScatter = {
  Double: [],
  Cards: [
    {
      Columns: [
        {
          Symbol: [8, 8, 8, 4, 7],
          RemoveAmount: 0,
        },
        {
          Symbol: [100, 7, 2, 6, 7],
          RemoveAmount: 0,
        },
        {
          Symbol: [2, 3, 5, 8, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 7, 6, 5, 5],
          RemoveAmount: 0,
        },
        {
          Symbol: [100, 1, 6, 6, 7],
          RemoveAmount: 0,
        },
        {
          Symbol: [2, 8, 0, 100, 4],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [],
      Payoff: [],
    },
  ],
  PromoActivity: {
    FreeRound: "0",
  },
  CustName: "twpgmsobichen",
  Balance: 100529.64,
  WinAmount: 118,
  FreeRound: 0,
};

export const hitFreeGame = {
  Scatter: { Amount: 3, FreeTimes: 10 },
  Double: [],
  Cards: [
    {
      Columns: [
        { Symbol: [100, 8, 8, 8, 2], RemoveAmount: 0 },
        { Symbol: [100, 7, 8, 2, 7], RemoveAmount: 0 },
        { Symbol: [100, 8, 7, 5, 2], RemoveAmount: 0 },
        { Symbol: [6, 5, 3, 8, 2], RemoveAmount: 0 },
        { Symbol: [5, 8, 7, 6, 6], RemoveAmount: 0 },
        { Symbol: [1, 7, 5, 4, 7], RemoveAmount: 0 },
      ],
      HitSymbol: [],
      HitAmount: [],
      Payoff: [],
    },
  ],
  PromoActivity: { FreeRound: "0" },
  FreeGame: [
    {
      TotalPayoff: 1.6,
      Scatter: { Amount: 5, FreeTimes: 12 },
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [1, 4, 2, 100, 1], RemoveAmount: 1 },
            { Symbol: [3, 3, 4, 2, 4], RemoveAmount: 2 },
            { Symbol: [3, 4, 7, 2, 3], RemoveAmount: 1 },
            { Symbol: [4, 1, 100, 1, 0], RemoveAmount: 1 },
            { Symbol: [1, 100, 4, 4, 3], RemoveAmount: 2 },
            { Symbol: [2, 4, 0, 1, 3], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [3, 1, 2, 100, 1], RemoveAmount: 1 },
            { Symbol: [100, 4, 3, 3, 2], RemoveAmount: 2 },
            { Symbol: [4, 3, 7, 2, 3], RemoveAmount: 2 },
            { Symbol: [3, 1, 100, 1, 0], RemoveAmount: 1 },
            { Symbol: [2, 4, 1, 100, 3], RemoveAmount: 1 },
            { Symbol: [0, 2, 0, 1, 3], RemoveAmount: 1 },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1.0],
        },
        {
          Columns: [
            { Symbol: [4, 1, 2, 100, 1], RemoveAmount: 0 },
            { Symbol: [0, 3, 100, 4, 2], RemoveAmount: 0 },
            { Symbol: [100, 4, 4, 7, 2], RemoveAmount: 0 },
            { Symbol: [5, 1, 100, 1, 0], RemoveAmount: 0 },
            { Symbol: [4, 2, 4, 1, 100], RemoveAmount: 0 },
            { Symbol: [3, 0, 2, 0, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 10.1,
      Scatter: 0,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 4, 2, 4, 4], RemoveAmount: 4 },
            { Symbol: [0, 0, 2, 2, 1], RemoveAmount: 0 },
            { Symbol: [0, 3, 4, 4, 4], RemoveAmount: 3 },
            { Symbol: [2, 4, 2, 3, 4], RemoveAmount: 2 },
            { Symbol: [0, 3, 0, 0, 4], RemoveAmount: 1 },
            { Symbol: [0, 4, 3, 3, 8], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [11],
          Payoff: [1.5],
        },
        {
          Columns: [
            { Symbol: [2, 0, 5, 0, 2], RemoveAmount: 2 },
            { Symbol: [0, 0, 2, 2, 1], RemoveAmount: 2 },
            { Symbol: [4, 4, 2, 0, 3], RemoveAmount: 1 },
            { Symbol: [4, 0, 2, 2, 3], RemoveAmount: 1 },
            { Symbol: [3, 0, 3, 0, 0], RemoveAmount: 3 },
            { Symbol: [1, 0, 3, 3, 8], RemoveAmount: 1 },
          ],
          HitSymbol: [0],
          HitAmount: [10],
          Payoff: [5.0],
        },
        {
          Columns: [
            { Symbol: [4, 0, 2, 5, 2], RemoveAmount: 1 },
            { Symbol: [4, 4, 2, 2, 1], RemoveAmount: 2 },
            { Symbol: [4, 4, 4, 2, 3], RemoveAmount: 3 },
            { Symbol: [3, 4, 2, 2, 3], RemoveAmount: 1 },
            { Symbol: [1, 0, 4, 3, 3], RemoveAmount: 1 },
            { Symbol: [4, 1, 3, 3, 8], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [0, 0, 2, 5, 2], RemoveAmount: 2 },
            { Symbol: [5, 4, 2, 2, 1], RemoveAmount: 2 },
            { Symbol: [3, 3, 2, 2, 3], RemoveAmount: 5 },
            { Symbol: [4, 3, 2, 2, 3], RemoveAmount: 4 },
            { Symbol: [2, 1, 0, 3, 3], RemoveAmount: 3 },
            { Symbol: [6, 1, 3, 3, 8], RemoveAmount: 2 },
          ],
          HitSymbol: [2, 3],
          HitAmount: [9, 9],
          Payoff: [2.0, 1.0],
        },
        {
          Columns: [
            { Symbol: [1, 3, 0, 0, 5], RemoveAmount: 0 },
            { Symbol: [3, 3, 5, 4, 1], RemoveAmount: 0 },
            { Symbol: [4, 1, 0, 4, 2], RemoveAmount: 0 },
            { Symbol: [5, 2, 2, 0, 4], RemoveAmount: 0 },
            { Symbol: [4, 4, 4, 1, 0], RemoveAmount: 0 },
            { Symbol: [1, 0, 6, 1, 8], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 9.1,
      Scatter: 4,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [3, 1, 3, 2, 2], RemoveAmount: 0 },
            { Symbol: [3, 3, 4, 100, 2], RemoveAmount: 1 },
            { Symbol: [100, 4, 8, 4, 1], RemoveAmount: 2 },
            { Symbol: [2, 2, 2, 3, 2], RemoveAmount: 0 },
            { Symbol: [8, 4, 1, 4, 4], RemoveAmount: 3 },
            { Symbol: [4, 6, 4, 7, 3], RemoveAmount: 2 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [3, 1, 3, 2, 2], RemoveAmount: 2 },
            { Symbol: [1, 3, 3, 100, 2], RemoveAmount: 1 },
            { Symbol: [0, 3, 100, 8, 1], RemoveAmount: 0 },
            { Symbol: [2, 2, 2, 3, 2], RemoveAmount: 4 },
            { Symbol: [6, 100, 2, 8, 1], RemoveAmount: 1 },
            { Symbol: [4, 4, 6, 7, 3], RemoveAmount: 0 },
          ],
          HitSymbol: [2],
          HitAmount: [8],
          Payoff: [2.0],
        },
        {
          Columns: [
            { Symbol: [1, 4, 3, 1, 3], RemoveAmount: 2 },
            { Symbol: [4, 1, 3, 3, 100], RemoveAmount: 2 },
            { Symbol: [0, 3, 100, 8, 1], RemoveAmount: 1 },
            { Symbol: [0, 5, 1, 3, 3], RemoveAmount: 2 },
            { Symbol: [3, 6, 100, 8, 1], RemoveAmount: 1 },
            { Symbol: [4, 4, 6, 7, 3], RemoveAmount: 1 },
          ],
          HitSymbol: [3],
          HitAmount: [9],
          Payoff: [1.0],
        },
        {
          Columns: [
            { Symbol: [1, 1, 1, 4, 1], RemoveAmount: 4 },
            { Symbol: [3, 2, 4, 1, 100], RemoveAmount: 1 },
            { Symbol: [2, 0, 100, 8, 1], RemoveAmount: 1 },
            { Symbol: [4, 1, 0, 5, 1], RemoveAmount: 2 },
            { Symbol: [2, 6, 100, 8, 1], RemoveAmount: 1 },
            { Symbol: [3, 4, 4, 6, 7], RemoveAmount: 0 },
          ],
          HitSymbol: [1],
          HitAmount: [9],
          Payoff: [3.0],
        },
        {
          Columns: [
            { Symbol: [100, 2, 3, 4, 4], RemoveAmount: 2 },
            { Symbol: [4, 3, 2, 4, 100], RemoveAmount: 2 },
            { Symbol: [4, 2, 0, 100, 8], RemoveAmount: 1 },
            { Symbol: [4, 4, 4, 0, 5], RemoveAmount: 3 },
            { Symbol: [1, 2, 6, 100, 8], RemoveAmount: 0 },
            { Symbol: [3, 4, 4, 6, 7], RemoveAmount: 2 },
          ],
          HitSymbol: [4],
          HitAmount: [10],
          Payoff: [1.5],
        },
        {
          Columns: [
            { Symbol: [3, 3, 100, 2, 3], RemoveAmount: 3 },
            { Symbol: [3, 1, 3, 2, 100], RemoveAmount: 2 },
            { Symbol: [0, 2, 0, 100, 8], RemoveAmount: 0 },
            { Symbol: [1, 4, 3, 0, 5], RemoveAmount: 1 },
            { Symbol: [1, 2, 6, 100, 8], RemoveAmount: 0 },
            { Symbol: [3, 0, 3, 6, 7], RemoveAmount: 2 },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1.0],
        },
        {
          Columns: [
            { Symbol: [1, 4, 0, 100, 2], RemoveAmount: 0 },
            { Symbol: [4, 3, 1, 2, 100], RemoveAmount: 0 },
            { Symbol: [0, 2, 0, 100, 8], RemoveAmount: 0 },
            { Symbol: [7, 1, 4, 0, 5], RemoveAmount: 0 },
            { Symbol: [1, 2, 6, 100, 8], RemoveAmount: 0 },
            { Symbol: [3, 1, 0, 6, 7], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.0,
      Scatter: 2,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [5, 2, 2, 4, 4], RemoveAmount: 0 },
            { Symbol: [1, 2, 0, 3, 0], RemoveAmount: 0 },
            { Symbol: [2, 4, 5, 3, 0], RemoveAmount: 0 },
            { Symbol: [4, 3, 3, 8, 6], RemoveAmount: 0 },
            { Symbol: [100, 0, 2, 2, 3], RemoveAmount: 0 },
            { Symbol: [100, 0, 8, 8, 8], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.6,
      Scatter: 2,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [5, 4, 4, 4, 3], RemoveAmount: 3 },
            { Symbol: [4, 4, 5, 1, 8], RemoveAmount: 2 },
            { Symbol: [3, 4, 3, 1, 8], RemoveAmount: 1 },
            { Symbol: [4, 1, 3, 100, 1], RemoveAmount: 1 },
            { Symbol: [0, 2, 100, 2, 1], RemoveAmount: 0 },
            { Symbol: [2, 3, 3, 1, 4], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [4, 4, 7, 5, 3], RemoveAmount: 0 },
            { Symbol: [1, 0, 5, 1, 8], RemoveAmount: 0 },
            { Symbol: [4, 3, 3, 1, 8], RemoveAmount: 0 },
            { Symbol: [3, 1, 3, 100, 1], RemoveAmount: 0 },
            { Symbol: [0, 2, 100, 2, 1], RemoveAmount: 0 },
            { Symbol: [4, 2, 3, 3, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 11.2,
      Scatter: 3,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [0, 3, 4, 0, 1], RemoveAmount: 2 },
            { Symbol: [4, 4, 0, 3, 100], RemoveAmount: 3 },
            { Symbol: [3, 8, 0, 4, 100], RemoveAmount: 2 },
            { Symbol: [4, 8, 4, 4, 0], RemoveAmount: 3 },
            { Symbol: [3, 1, 3, 3, 3], RemoveAmount: 4 },
            { Symbol: [4, 3, 2, 3, 5], RemoveAmount: 3 },
          ],
          HitSymbol: [3, 4],
          HitAmount: [9, 8],
          Payoff: [1.0, 0.6],
        },
        {
          Columns: [
            { Symbol: [7, 0, 0, 0, 1], RemoveAmount: 3 },
            { Symbol: [2, 4, 0, 0, 100], RemoveAmount: 2 },
            { Symbol: [1, 1, 8, 0, 100], RemoveAmount: 1 },
            { Symbol: [6, 4, 2, 8, 0], RemoveAmount: 1 },
            { Symbol: [6, 1, 100, 0, 1], RemoveAmount: 1 },
            { Symbol: [4, 4, 1, 2, 5], RemoveAmount: 0 },
          ],
          HitSymbol: [0],
          HitAmount: [8],
          Payoff: [4.0],
        },
        {
          Columns: [
            { Symbol: [4, 4, 3, 7, 1], RemoveAmount: 2 },
            { Symbol: [4, 3, 2, 4, 100], RemoveAmount: 2 },
            { Symbol: [5, 1, 1, 8, 100], RemoveAmount: 0 },
            { Symbol: [4, 6, 4, 2, 8], RemoveAmount: 2 },
            { Symbol: [1, 6, 1, 100, 1], RemoveAmount: 0 },
            { Symbol: [4, 4, 1, 2, 5], RemoveAmount: 2 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [2, 3, 3, 7, 1], RemoveAmount: 1 },
            { Symbol: [8, 3, 3, 2, 100], RemoveAmount: 0 },
            { Symbol: [5, 1, 1, 8, 100], RemoveAmount: 2 },
            { Symbol: [2, 3, 6, 2, 8], RemoveAmount: 0 },
            { Symbol: [1, 6, 1, 100, 1], RemoveAmount: 3 },
            { Symbol: [3, 1, 1, 2, 5], RemoveAmount: 2 },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3.0],
        },
        {
          Columns: [
            { Symbol: [3, 2, 3, 3, 7], RemoveAmount: 3 },
            { Symbol: [8, 3, 3, 2, 100], RemoveAmount: 2 },
            { Symbol: [4, 3, 5, 8, 100], RemoveAmount: 1 },
            { Symbol: [2, 3, 6, 2, 8], RemoveAmount: 1 },
            { Symbol: [4, 1, 4, 6, 100], RemoveAmount: 0 },
            { Symbol: [3, 3, 3, 2, 5], RemoveAmount: 3 },
          ],
          HitSymbol: [3],
          HitAmount: [10],
          Payoff: [2.0],
        },
        {
          Columns: [
            { Symbol: [5, 4, 4, 2, 7], RemoveAmount: 0 },
            { Symbol: [6, 0, 8, 2, 100], RemoveAmount: 0 },
            { Symbol: [1, 4, 5, 8, 100], RemoveAmount: 0 },
            { Symbol: [2, 2, 6, 2, 8], RemoveAmount: 0 },
            { Symbol: [4, 1, 4, 6, 100], RemoveAmount: 0 },
            { Symbol: [1, 6, 4, 2, 5], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.0,
      Scatter: 0,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 2, 0, 1, 3], RemoveAmount: 0 },
            { Symbol: [3, 2, 8, 2, 4], RemoveAmount: 0 },
            { Symbol: [3, 0, 2, 2, 6], RemoveAmount: 0 },
            { Symbol: [0, 3, 0, 3, 4], RemoveAmount: 0 },
            { Symbol: [2, 3, 4, 1, 4], RemoveAmount: 0 },
            { Symbol: [1, 4, 0, 2, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.6,
      Scatter: 2,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 4, 8, 4, 0], RemoveAmount: 3 },
            { Symbol: [3, 100, 4, 4, 2], RemoveAmount: 2 },
            { Symbol: [8, 8, 2, 3, 2], RemoveAmount: 0 },
            { Symbol: [4, 6, 5, 8, 7], RemoveAmount: 1 },
            { Symbol: [4, 2, 3, 8, 4], RemoveAmount: 2 },
            { Symbol: [3, 2, 2, 1, 5], RemoveAmount: 0 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [2, 3, 4, 8, 0], RemoveAmount: 0 },
            { Symbol: [3, 0, 3, 100, 2], RemoveAmount: 0 },
            { Symbol: [8, 8, 2, 3, 2], RemoveAmount: 0 },
            { Symbol: [3, 6, 5, 8, 7], RemoveAmount: 0 },
            { Symbol: [4, 100, 2, 3, 8], RemoveAmount: 0 },
            { Symbol: [3, 2, 2, 1, 5], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.6,
      Scatter: 2,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [100, 4, 4, 8, 5], RemoveAmount: 2 },
            { Symbol: [2, 4, 3, 1, 4], RemoveAmount: 2 },
            { Symbol: [4, 2, 0, 3, 4], RemoveAmount: 2 },
            { Symbol: [0, 0, 0, 2, 8], RemoveAmount: 0 },
            { Symbol: [4, 1, 8, 2, 4], RemoveAmount: 2 },
            { Symbol: [3, 4, 2, 1, 0], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [1, 4, 100, 8, 5], RemoveAmount: 0 },
            { Symbol: [8, 2, 2, 3, 1], RemoveAmount: 0 },
            { Symbol: [3, 100, 2, 0, 3], RemoveAmount: 0 },
            { Symbol: [0, 0, 0, 2, 8], RemoveAmount: 0 },
            { Symbol: [3, 3, 1, 8, 2], RemoveAmount: 0 },
            { Symbol: [4, 3, 2, 1, 0], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 1.6,
      Scatter: 0,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 4, 4, 1, 3], RemoveAmount: 3 },
            { Symbol: [0, 7, 2, 8, 6], RemoveAmount: 0 },
            { Symbol: [3, 3, 4, 3, 3], RemoveAmount: 1 },
            { Symbol: [1, 1, 1, 2, 2], RemoveAmount: 0 },
            { Symbol: [1, 4, 4, 4, 3], RemoveAmount: 3 },
            { Symbol: [8, 3, 4, 1, 2], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [4, 7, 0, 1, 3], RemoveAmount: 1 },
            { Symbol: [0, 7, 2, 8, 6], RemoveAmount: 0 },
            { Symbol: [3, 3, 3, 3, 3], RemoveAmount: 5 },
            { Symbol: [1, 1, 1, 2, 2], RemoveAmount: 0 },
            { Symbol: [8, 0, 4, 1, 3], RemoveAmount: 1 },
            { Symbol: [4, 8, 3, 1, 2], RemoveAmount: 1 },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1.0],
        },
        {
          Columns: [
            { Symbol: [3, 4, 7, 0, 1], RemoveAmount: 0 },
            { Symbol: [0, 7, 2, 8, 6], RemoveAmount: 0 },
            { Symbol: [3, 4, 3, 0, 0], RemoveAmount: 0 },
            { Symbol: [1, 1, 1, 2, 2], RemoveAmount: 0 },
            { Symbol: [4, 8, 0, 4, 1], RemoveAmount: 0 },
            { Symbol: [2, 4, 8, 1, 2], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.0,
      Scatter: 1,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [3, 1, 3, 3, 1], RemoveAmount: 0 },
            { Symbol: [1, 3, 8, 4, 2], RemoveAmount: 0 },
            { Symbol: [3, 8, 6, 1, 5], RemoveAmount: 0 },
            { Symbol: [4, 4, 4, 1, 4], RemoveAmount: 0 },
            { Symbol: [100, 3, 0, 4, 4], RemoveAmount: 0 },
            { Symbol: [0, 2, 1, 5, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.0,
      Scatter: 1,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 1, 0, 3, 0], RemoveAmount: 0 },
            { Symbol: [100, 2, 4, 1, 1], RemoveAmount: 0 },
            { Symbol: [4, 4, 2, 3, 2], RemoveAmount: 0 },
            { Symbol: [3, 3, 3, 0, 2], RemoveAmount: 0 },
            { Symbol: [1, 0, 1, 8, 4], RemoveAmount: 0 },
            { Symbol: [2, 6, 3, 0, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 3.6,
      Scatter: 1,
      Double: [6],
      Cards: [
        {
          Columns: [
            { Symbol: [3, 3, 3, 1, 1], RemoveAmount: 0 },
            { Symbol: [3, 4, 1, 96, 4], RemoveAmount: 2 },
            { Symbol: [5, 0, 4, 4, 4], RemoveAmount: 3 },
            { Symbol: [4, 0, 3, 2, 1], RemoveAmount: 1 },
            { Symbol: [4, 2, 4, 8, 100], RemoveAmount: 2 },
            { Symbol: [2, 3, 2, 8, 2], RemoveAmount: 0 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [3, 3, 3, 1, 1], RemoveAmount: 0 },
            { Symbol: [4, 2, 3, 1, 96], RemoveAmount: 0 },
            { Symbol: [8, 6, 0, 5, 0], RemoveAmount: 0 },
            { Symbol: [7, 0, 3, 2, 1], RemoveAmount: 0 },
            { Symbol: [1, 0, 2, 8, 100], RemoveAmount: 0 },
            { Symbol: [2, 3, 2, 8, 2], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.6,
      Scatter: 3,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [3, 0, 0, 1, 4], RemoveAmount: 1 },
            { Symbol: [5, 0, 2, 4, 5], RemoveAmount: 1 },
            { Symbol: [2, 3, 4, 4, 4], RemoveAmount: 3 },
            { Symbol: [1, 3, 0, 100, 3], RemoveAmount: 0 },
            { Symbol: [1, 1, 4, 3, 2], RemoveAmount: 1 },
            { Symbol: [3, 4, 1, 4, 100], RemoveAmount: 2 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [1, 3, 0, 0, 1], RemoveAmount: 0 },
            { Symbol: [4, 5, 0, 2, 5], RemoveAmount: 0 },
            { Symbol: [4, 4, 100, 2, 3], RemoveAmount: 0 },
            { Symbol: [1, 3, 0, 100, 3], RemoveAmount: 0 },
            { Symbol: [4, 1, 1, 3, 2], RemoveAmount: 0 },
            { Symbol: [1, 4, 3, 1, 100], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 12.2,
      Scatter: 2,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [6, 3, 2, 2, 8], RemoveAmount: 2 },
            { Symbol: [4, 2, 4, 0, 2], RemoveAmount: 2 },
            { Symbol: [0, 4, 2, 100, 2], RemoveAmount: 2 },
            { Symbol: [1, 0, 0, 1, 4], RemoveAmount: 0 },
            { Symbol: [0, 3, 100, 4, 4], RemoveAmount: 0 },
            { Symbol: [3, 3, 3, 2, 2], RemoveAmount: 2 },
          ],
          HitSymbol: [2],
          HitAmount: [8],
          Payoff: [2.0],
        },
        {
          Columns: [
            { Symbol: [4, 3, 6, 3, 8], RemoveAmount: 1 },
            { Symbol: [2, 0, 4, 4, 0], RemoveAmount: 2 },
            { Symbol: [4, 4, 0, 4, 100], RemoveAmount: 3 },
            { Symbol: [1, 0, 0, 1, 4], RemoveAmount: 1 },
            { Symbol: [0, 3, 100, 4, 4], RemoveAmount: 2 },
            { Symbol: [1, 3, 3, 3, 3], RemoveAmount: 0 },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [3, 3, 6, 3, 8], RemoveAmount: 3 },
            { Symbol: [4, 0, 2, 0, 0], RemoveAmount: 0 },
            { Symbol: [3, 1, 3, 0, 100], RemoveAmount: 2 },
            { Symbol: [3, 1, 0, 0, 1], RemoveAmount: 1 },
            { Symbol: [4, 1, 0, 3, 100], RemoveAmount: 1 },
            { Symbol: [1, 3, 3, 3, 3], RemoveAmount: 4 },
          ],
          HitSymbol: [3],
          HitAmount: [11],
          Payoff: [2.0],
        },
        {
          Columns: [
            { Symbol: [3, 1, 2, 6, 8], RemoveAmount: 1 },
            { Symbol: [4, 0, 2, 0, 0], RemoveAmount: 0 },
            { Symbol: [6, 1, 1, 0, 100], RemoveAmount: 2 },
            { Symbol: [2, 1, 0, 0, 1], RemoveAmount: 2 },
            { Symbol: [1, 4, 1, 0, 100], RemoveAmount: 2 },
            { Symbol: [4, 4, 3, 4, 1], RemoveAmount: 1 },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3.0],
        },
        {
          Columns: [
            { Symbol: [2, 3, 2, 6, 8], RemoveAmount: 0 },
            { Symbol: [4, 0, 2, 0, 0], RemoveAmount: 1 },
            { Symbol: [1, 6, 6, 0, 100], RemoveAmount: 0 },
            { Symbol: [4, 4, 2, 0, 0], RemoveAmount: 2 },
            { Symbol: [3, 4, 4, 0, 100], RemoveAmount: 2 },
            { Symbol: [3, 4, 4, 3, 4], RemoveAmount: 3 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [2, 3, 2, 6, 8], RemoveAmount: 0 },
            { Symbol: [4, 0, 2, 0, 0], RemoveAmount: 3 },
            { Symbol: [1, 6, 6, 0, 100], RemoveAmount: 1 },
            { Symbol: [0, 2, 2, 0, 0], RemoveAmount: 3 },
            { Symbol: [3, 4, 3, 0, 100], RemoveAmount: 1 },
            { Symbol: [4, 1, 1, 3, 3], RemoveAmount: 0 },
          ],
          HitSymbol: [0],
          HitAmount: [8],
          Payoff: [4.0],
        },
        {
          Columns: [
            { Symbol: [2, 3, 2, 6, 8], RemoveAmount: 0 },
            { Symbol: [4, 3, 6, 4, 2], RemoveAmount: 0 },
            { Symbol: [2, 1, 6, 6, 100], RemoveAmount: 0 },
            { Symbol: [6, 4, 0, 2, 2], RemoveAmount: 0 },
            { Symbol: [4, 3, 4, 3, 100], RemoveAmount: 0 },
            { Symbol: [4, 1, 1, 3, 3], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.6,
      Scatter: 1,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [2, 4, 4, 6, 3], RemoveAmount: 2 },
            { Symbol: [4, 0, 0, 4, 4], RemoveAmount: 3 },
            { Symbol: [2, 8, 4, 3, 3], RemoveAmount: 1 },
            { Symbol: [1, 0, 3, 2, 5], RemoveAmount: 0 },
            { Symbol: [0, 4, 5, 1, 2], RemoveAmount: 1 },
            { Symbol: [4, 100, 0, 2, 1], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [4, 2, 2, 6, 3], RemoveAmount: 0 },
            { Symbol: [2, 0, 0, 0, 0], RemoveAmount: 0 },
            { Symbol: [4, 2, 8, 3, 3], RemoveAmount: 0 },
            { Symbol: [1, 0, 3, 2, 5], RemoveAmount: 0 },
            { Symbol: [4, 0, 5, 1, 2], RemoveAmount: 0 },
            { Symbol: [4, 100, 0, 2, 1], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 9.2,
      Scatter: 3,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [4, 2, 100, 2, 1], RemoveAmount: 3 },
            { Symbol: [1, 0, 4, 2, 1], RemoveAmount: 2 },
            { Symbol: [4, 0, 2, 4, 2], RemoveAmount: 4 },
            { Symbol: [2, 100, 3, 4, 2], RemoveAmount: 3 },
            { Symbol: [3, 1, 4, 3, 4], RemoveAmount: 2 },
            { Symbol: [4, 2, 1, 8, 1], RemoveAmount: 2 },
          ],
          HitSymbol: [4, 2],
          HitAmount: [8, 8],
          Payoff: [0.6, 2.0],
        },
        {
          Columns: [
            { Symbol: [4, 3, 4, 100, 1], RemoveAmount: 2 },
            { Symbol: [4, 4, 1, 0, 1], RemoveAmount: 2 },
            { Symbol: [6, 4, 4, 100, 0], RemoveAmount: 2 },
            { Symbol: [4, 8, 2, 100, 3], RemoveAmount: 1 },
            { Symbol: [2, 2, 3, 1, 3], RemoveAmount: 0 },
            { Symbol: [2, 4, 1, 8, 1], RemoveAmount: 1 },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            { Symbol: [6, 2, 3, 100, 1], RemoveAmount: 1 },
            { Symbol: [4, 8, 1, 0, 1], RemoveAmount: 2 },
            { Symbol: [4, 1, 6, 100, 0], RemoveAmount: 1 },
            { Symbol: [1, 8, 2, 100, 3], RemoveAmount: 1 },
            { Symbol: [2, 2, 3, 1, 3], RemoveAmount: 1 },
            { Symbol: [4, 2, 1, 8, 1], RemoveAmount: 2 },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3.0],
        },
        {
          Columns: [
            { Symbol: [4, 6, 2, 3, 100], RemoveAmount: 1 },
            { Symbol: [2, 0, 4, 8, 0], RemoveAmount: 1 },
            { Symbol: [4, 4, 6, 100, 0], RemoveAmount: 0 },
            { Symbol: [2, 8, 2, 100, 3], RemoveAmount: 2 },
            { Symbol: [3, 2, 2, 3, 3], RemoveAmount: 2 },
            { Symbol: [2, 3, 4, 2, 8], RemoveAmount: 2 },
          ],
          HitSymbol: [2],
          HitAmount: [8],
          Payoff: [2.0],
        },
        {
          Columns: [
            { Symbol: [2, 4, 6, 3, 100], RemoveAmount: 1 },
            { Symbol: [3, 0, 4, 8, 0], RemoveAmount: 1 },
            { Symbol: [4, 4, 6, 100, 0], RemoveAmount: 0 },
            { Symbol: [0, 2, 8, 100, 3], RemoveAmount: 1 },
            { Symbol: [2, 3, 3, 3, 3], RemoveAmount: 4 },
            { Symbol: [2, 1, 3, 4, 8], RemoveAmount: 1 },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1.0],
        },
        {
          Columns: [
            { Symbol: [1, 2, 4, 6, 100], RemoveAmount: 0 },
            { Symbol: [3, 0, 4, 8, 0], RemoveAmount: 0 },
            { Symbol: [4, 4, 6, 100, 0], RemoveAmount: 0 },
            { Symbol: [0, 0, 2, 8, 100], RemoveAmount: 0 },
            { Symbol: [6, 0, 4, 2, 2], RemoveAmount: 0 },
            { Symbol: [0, 2, 1, 4, 8], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      PayOff: 0.0,
      Scatter: 1,
      Double: [],
      Cards: [
        {
          Columns: [
            { Symbol: [100, 4, 6, 4, 0], RemoveAmount: 0 },
            { Symbol: [4, 1, 1, 3, 3], RemoveAmount: 0 },
            { Symbol: [2, 1, 0, 0, 3], RemoveAmount: 0 },
            { Symbol: [2, 4, 2, 4, 7], RemoveAmount: 0 },
            { Symbol: [2, 0, 3, 1, 3], RemoveAmount: 0 },
            { Symbol: [3, 0, 0, 3, 4], RemoveAmount: 0 },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
  ],
  CustName: "Test",
  Balance: 0.0,
  TotalPayoff: 0.0,
};

export const hitFreeGameWithPayoff = {
  CustName: "twpgmsobichen",
  Balance: 109820.34,
  TotalPayoff: 0.3,
  Scatter: {
    Amount: 3,
    FreeTimes: 10,
  },
  Double: [],
  Cards: [
    {
      Columns: [
        {
          Symbol: [100, 3, 7, 7, 8],
          RemoveAmount: 2,
        },
        {
          Symbol: [100, 4, 6, 6, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [100, 6, 7, 7, 7],
          RemoveAmount: 3,
        },
        {
          Symbol: [1, 7, 8, 6, 7],
          RemoveAmount: 2,
        },
        {
          Symbol: [6, 4, 3, 3, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 7, 0, 2, 2],
          RemoveAmount: 1,
        },
      ],
      HitSymbol: [7],
      HitAmount: [8],
      Payoff: [],
    },
    {
      Columns: [
        {
          Symbol: [5, 0, 100, 3, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [100, 4, 6, 6, 8],
          RemoveAmount: 0,
        },
        {
          Symbol: [8, 8, 7, 100, 6],
          RemoveAmount: 0,
        },
        {
          Symbol: [1, 7, 1, 8, 6],
          RemoveAmount: 0,
        },
        {
          Symbol: [6, 4, 3, 3, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [4, 6, 0, 2, 2],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [],
      HitAmount: [],
      Payoff: [],
    },
  ],
  PromoActivity: {
    FreeRound: "0",
  },
  FreeGame: [
    {
      TotalPayOff: 0.6,
      Scatter: {
        Amount: 2,
        FreeTimes: 9,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [0, 3, 3, 7, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [6, 4, 3, 4, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [4, 4, 3, 5, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [0, 8, 100, 0, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [100, 2, 4, 2, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 3, 5, 4, 0],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [0, 3, 3, 7, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 3, 1, 6, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 2, 3, 5, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 8, 100, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 100, 2, 2, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 2, 3, 5, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 4.6,
      Scatter: {
        Amount: 5,
        FreeTimes: 11,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [8, 4, 7, 4, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [4, 2, 4, 2, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 4, 3, 100, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 2, 100, 3, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 100, 4, 1, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 3, 0, 2, 100],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [2, 0, 2, 8, 7],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 3, 2, 2, 3],
              RemoveAmount: 5,
            },
            {
              Symbol: [2, 2, 3, 100, 2],
              RemoveAmount: 4,
            },
            {
              Symbol: [1, 2, 100, 3, 3],
              RemoveAmount: 3,
            },
            {
              Symbol: [4, 0, 100, 1, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 3, 0, 2, 100],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [2, 3],
          HitAmount: [10, 8],
          Payoff: [3, 1],
        },
        {
          Columns: [
            {
              Symbol: [0, 6, 0, 8, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 2, 100, 3, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 3, 4, 6, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 1, 8, 1, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 0, 100, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 4, 2, 0, 100],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 5.6,
      Scatter: {
        Amount: 2,
        FreeTimes: 10,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 2, 3, 1, 0],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 4, 0, 4, 2],
              RemoveAmount: 2,
            },
            {
              Symbol: [5, 4, 2, 8, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 1, 2, 4, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 1, 1, 6, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 6, 4, 4, 4],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [100, 2, 3, 1, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 1, 2, 0, 2],
              RemoveAmount: 3,
            },
            {
              Symbol: [1, 5, 2, 8, 2],
              RemoveAmount: 3,
            },
            {
              Symbol: [0, 4, 1, 2, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 1, 1, 6, 1],
              RemoveAmount: 4,
            },
            {
              Symbol: [1, 2, 3, 3, 6],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [2, 1],
          HitAmount: [8, 8],
          Payoff: [2, 3],
        },
        {
          Columns: [
            {
              Symbol: [6, 6, 100, 3, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 1, 1, 3, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [7, 1, 2, 5, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 0, 4, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 1, 100, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 3, 3, 3, 6],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 0,
      Scatter: {
        Amount: 2,
        FreeTimes: 9,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 4, 4, 0, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 1, 100, 0, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 2, 4, 1, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 1, 4, 3, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [100, 4, 6, 8, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 2, 4, 3, 3],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 1.5,
      Scatter: {
        Amount: 2,
        FreeTimes: 8,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 4, 4, 4, 0],
              RemoveAmount: 4,
            },
            {
              Symbol: [2, 1, 0, 4, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 5, 4, 2, 8],
              RemoveAmount: 1,
            },
            {
              Symbol: [100, 8, 4, 3, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 3, 1, 0, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 4, 3, 100, 0],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [10],
          Payoff: [1.5],
        },
        {
          Columns: [
            {
              Symbol: [1, 0, 4, 4, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 2, 1, 0, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 0, 5, 2, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 100, 8, 3, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 3, 1, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [8, 2, 3, 100, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 2.6,
      Scatter: {
        Amount: 1,
        FreeTimes: 7,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [2, 3, 2, 2, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 3, 4, 0, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 6, 4, 4, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 2, 2, 0, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 1, 1, 4, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 3, 1, 3, 0],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [3],
          HitAmount: [10],
          Payoff: [2],
        },
        {
          Columns: [
            {
              Symbol: [0, 2, 2, 2, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 1, 4, 0, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 6, 4, 4, 100],
              RemoveAmount: 3,
            },
            {
              Symbol: [3, 4, 2, 2, 0],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 3, 1, 1, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [7, 1, 1, 1, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [6, 0, 2, 2, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 8, 1, 0, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 3, 6, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 3, 2, 2, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 2, 3, 1, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [7, 1, 1, 1, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 4.5,
      Scatter: {
        Amount: 2,
        FreeTimes: 6,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [1, 4, 100, 1, 0],
              RemoveAmount: 1,
            },
            {
              Symbol: [7, 0, 4, 1, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [5, 1, 6, 6, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 3, 4, 4, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [100, 1, 2, 0, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 4, 4, 2, 1],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [4],
          HitAmount: [11],
          Payoff: [1.5],
        },
        {
          Columns: [
            {
              Symbol: [3, 1, 100, 1, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 4, 7, 0, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [5, 5, 1, 6, 6],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 1, 3, 0, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 100, 1, 2, 0],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 5, 4, 2, 1],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3],
        },
        {
          Columns: [
            {
              Symbol: [1, 0, 3, 100, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 1, 4, 7, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 5, 5, 6, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 3, 3, 0, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 2, 100, 2, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 5, 4, 2],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 14.7,
      Scatter: {
        Amount: 3,
        FreeTimes: 6,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 4, 2, 4, 1],
              RemoveAmount: 3,
            },
            {
              Symbol: [0, 4, 3, 100, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [1, 1, 4, 3, 3],
              RemoveAmount: 3,
            },
            {
              Symbol: [3, 3, 3, 1, 3],
              RemoveAmount: 4,
            },
            {
              Symbol: [2, 4, 4, 4, 3],
              RemoveAmount: 4,
            },
            {
              Symbol: [0, 4, 5, 100, 4],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [4, 3],
          HitAmount: [11, 8],
          Payoff: [1.5, 1],
        },
        {
          Columns: [
            {
              Symbol: [3, 8, 3, 2, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 4, 4, 0, 100],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 4, 0, 1, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 1, 7, 4, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 4, 4, 2, 2],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 0, 0, 5, 100],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [3, 8, 3, 2, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 4, 2, 0, 100],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 5, 0, 1, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 1, 1, 7, 1],
              RemoveAmount: 3,
            },
            {
              Symbol: [4, 4, 1, 2, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 0, 0, 5, 100],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [1],
          HitAmount: [9],
          Payoff: [3],
        },
        {
          Columns: [
            {
              Symbol: [2, 3, 8, 3, 2],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 4, 2, 0, 100],
              RemoveAmount: 2,
            },
            {
              Symbol: [0, 0, 4, 5, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 4, 4, 3, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 4, 4, 2, 2],
              RemoveAmount: 3,
            },
            {
              Symbol: [2, 0, 0, 5, 100],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [2],
          HitAmount: [8],
          Payoff: [2],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 8, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 3, 4, 0, 100],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 0, 4, 5, 0],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 4, 4, 3, 7],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 7, 4, 4, 4],
              RemoveAmount: 4,
            },
            {
              Symbol: [4, 0, 0, 5, 100],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 8, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 2, 3, 0, 100],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 0, 0, 5, 0],
              RemoveAmount: 3,
            },
            {
              Symbol: [5, 0, 0, 3, 7],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 7, 4, 1, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 0, 0, 5, 100],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [0],
          HitAmount: [8],
          Payoff: [4],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 8, 3],
              RemoveAmount: 4,
            },
            {
              Symbol: [0, 4, 2, 3, 100],
              RemoveAmount: 1,
            },
            {
              Symbol: [6, 3, 3, 1, 5],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 4, 5, 3, 7],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 7, 4, 1, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 3, 3, 5, 100],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [3],
          HitAmount: [10],
          Payoff: [2],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 0, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 4, 2, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 3, 6, 1, 5],
              RemoveAmount: 0,
            },
            {
              Symbol: [100, 4, 4, 5, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 7, 4, 1, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [6, 1, 0, 5, 100],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 0,
      Scatter: {
        Amount: 1,
        FreeTimes: 5,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [3, 6, 0, 0, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [7, 8, 1, 100, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 1, 2, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 2, 4, 1, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [6, 0, 4, 2, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 1, 2, 3, 6],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 1.6,
      Scatter: {
        Amount: 2,
        FreeTimes: 4,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [8, 8, 4, 4, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 0, 100, 4, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 0, 4, 3, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 1, 5, 3, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 7, 3, 2, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 4, 4, 3, 7],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [4, 4, 8, 8, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 1, 0, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 3, 0, 3, 3],
              RemoveAmount: 3,
            },
            {
              Symbol: [2, 1, 5, 3, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [6, 0, 7, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 4, 3, 3, 7],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1],
        },
        {
          Columns: [
            {
              Symbol: [4, 4, 8, 8, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 1, 0, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 4, 4, 0, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 2, 2, 1, 5],
              RemoveAmount: 0,
            },
            {
              Symbol: [6, 6, 0, 7, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [8, 100, 4, 4, 7],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 1.5,
      Scatter: {
        Amount: 2,
        FreeTimes: 3,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 3, 4, 7, 6],
              RemoveAmount: 2,
            },
            {
              Symbol: [100, 0, 4, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 4, 2, 0, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 4, 4, 100, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 4, 4, 1, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 8, 7, 2, 7],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [10],
          Payoff: [1.5],
        },
        {
          Columns: [
            {
              Symbol: [6, 4, 3, 7, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [8, 100, 0, 3, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 1, 2, 0, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 3, 1, 100, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 6, 1, 1, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 8, 7, 2, 7],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 3.6,
      Scatter: {
        Amount: 2,
        FreeTimes: 2,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 4, 2, 4, 7],
              RemoveAmount: 3,
            },
            {
              Symbol: [2, 2, 1, 0, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 3, 2, 4, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 1, 4, 1, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 7, 0, 2, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [100, 1, 7, 4, 0],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [4, 1, 0, 2, 7],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 2, 1, 0, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [100, 3, 1, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 3, 3, 1, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 7, 0, 2, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [7, 100, 1, 7, 0],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3],
        },
        {
          Columns: [
            {
              Symbol: [2, 4, 0, 2, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [7, 3, 2, 2, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 100, 3, 3, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 3, 0, 3, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 7, 0, 2, 3],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 7, 100, 7, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 0,
      Scatter: {
        Amount: 3,
        FreeTimes: 2,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [8, 2, 100, 3, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 4, 7, 3, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 2, 4, 0, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 3, 100, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 100, 3, 2, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 3, 0, 3, 2],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 16.2,
      Scatter: {
        Amount: 4,
        FreeTimes: 3,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [4, 1, 3, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 0, 2, 3, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [100, 0, 4, 3, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 4, 2, 1, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 0, 1, 4, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 2, 0, 2, 4],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [8],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [3, 1, 3, 3, 2],
              RemoveAmount: 3,
            },
            {
              Symbol: [0, 3, 0, 2, 3],
              RemoveAmount: 2,
            },
            {
              Symbol: [2, 100, 0, 3, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 1, 0, 2, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 3, 0, 1, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 0, 2, 0, 2],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 4, 1, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 1, 0, 0, 2],
              RemoveAmount: 3,
            },
            {
              Symbol: [8, 2, 100, 0, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [1, 1, 0, 2, 1],
              RemoveAmount: 4,
            },
            {
              Symbol: [2, 4, 0, 1, 1],
              RemoveAmount: 3,
            },
            {
              Symbol: [0, 0, 2, 0, 2],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [1, 0],
          HitAmount: [8, 8],
          Payoff: [3, 4],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 4, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 3, 3, 4, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 0, 8, 2, 100],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 2, 4, 2, 2],
              RemoveAmount: 4,
            },
            {
              Symbol: [4, 100, 1, 2, 4],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 100, 2, 2, 2],
              RemoveAmount: 4,
            },
          ],
          HitSymbol: [2],
          HitAmount: [12],
          Payoff: [6],
        },
        {
          Columns: [
            {
              Symbol: [3, 3, 3, 3, 4],
              RemoveAmount: 5,
            },
            {
              Symbol: [4, 3, 3, 3, 4],
              RemoveAmount: 5,
            },
            {
              Symbol: [6, 1, 0, 8, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 4, 2, 3, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [4, 4, 100, 1, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [5, 3, 2, 0, 100],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [3, 4],
          HitAmount: [9, 8],
          Payoff: [1, 0.6],
        },
        {
          Columns: [
            {
              Symbol: [0, 0, 3, 5, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 2, 3, 2, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [6, 1, 0, 8, 100],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 5, 4, 1, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 3, 6, 100, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 5, 2, 0, 100],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 0,
      Scatter: {
        Amount: 3,
        FreeTimes: 3,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [2, 3, 3, 4, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [8, 2, 100, 3, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 2, 100, 4, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 1, 3, 4, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 0, 100, 2, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 3, 3, 4, 4],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 0,
      Scatter: {
        Amount: 0,
        FreeTimes: 2,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [1, 3, 1, 2, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 2, 3, 0, 2],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 5, 0, 0, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 2, 1, 8, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 2, 6, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 3, 0, 3, 1],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 8.1,
      Scatter: {
        Amount: 1,
        FreeTimes: 1,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [3, 2, 3, 4, 1],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 4, 4, 2, 4],
              RemoveAmount: 4,
            },
            {
              Symbol: [2, 4, 2, 5, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 4, 8, 5, 8],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 1, 7, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [7, 4, 1, 3, 1],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [4],
          HitAmount: [9],
          Payoff: [0.6],
        },
        {
          Columns: [
            {
              Symbol: [2, 3, 2, 3, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 6, 3, 4, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [100, 2, 2, 5, 3],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 1, 8, 5, 8],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 1, 7, 3, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 7, 1, 3, 1],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [3],
          HitAmount: [8],
          Payoff: [1],
        },
        {
          Columns: [
            {
              Symbol: [4, 2, 2, 2, 1],
              RemoveAmount: 3,
            },
            {
              Symbol: [3, 4, 6, 4, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [2, 100, 2, 2, 5],
              RemoveAmount: 3,
            },
            {
              Symbol: [1, 1, 8, 5, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 1, 1, 7, 2],
              RemoveAmount: 1,
            },
            {
              Symbol: [4, 2, 7, 1, 1],
              RemoveAmount: 1,
            },
          ],
          HitSymbol: [2],
          HitAmount: [9],
          Payoff: [2],
        },
        {
          Columns: [
            {
              Symbol: [1, 2, 0, 4, 1],
              RemoveAmount: 2,
            },
            {
              Symbol: [3, 3, 4, 6, 4],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 4, 2, 100, 5],
              RemoveAmount: 0,
            },
            {
              Symbol: [1, 1, 8, 5, 8],
              RemoveAmount: 2,
            },
            {
              Symbol: [0, 3, 1, 1, 7],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 4, 7, 1, 1],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [1],
          HitAmount: [8],
          Payoff: [3],
        },
        {
          Columns: [
            {
              Symbol: [4, 4, 2, 0, 4],
              RemoveAmount: 3,
            },
            {
              Symbol: [3, 3, 4, 6, 4],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 4, 2, 100, 5],
              RemoveAmount: 2,
            },
            {
              Symbol: [4, 8, 8, 5, 8],
              RemoveAmount: 1,
            },
            {
              Symbol: [1, 4, 0, 3, 7],
              RemoveAmount: 1,
            },
            {
              Symbol: [0, 0, 4, 4, 7],
              RemoveAmount: 2,
            },
          ],
          HitSymbol: [4],
          HitAmount: [11],
          Payoff: [1.5],
        },
        {
          Columns: [
            {
              Symbol: [1, 6, 1, 2, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [7, 4, 3, 3, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 2, 2, 100, 5],
              RemoveAmount: 0,
            },
            {
              Symbol: [5, 8, 8, 5, 8],
              RemoveAmount: 0,
            },
            {
              Symbol: [4, 1, 0, 3, 7],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 0, 0, 0, 7],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
    {
      TotalPayOff: 1.6,
      Scatter: {
        Amount: 1,
        FreeTimes: 0,
      },
      Double: [],
      Cards: [
        {
          Columns: [
            {
              Symbol: [3, 4, 4, 6, 4],
              RemoveAmount: 4,
            },
            {
              Symbol: [3, 0, 4, 3, 1],
              RemoveAmount: 3,
            },
            {
              Symbol: [1, 4, 4, 3, 4],
              RemoveAmount: 4,
            },
            {
              Symbol: [3, 100, 4, 5, 0],
              RemoveAmount: 2,
            },
            {
              Symbol: [0, 1, 3, 0, 6],
              RemoveAmount: 1,
            },
            {
              Symbol: [3, 2, 0, 4, 3],
              RemoveAmount: 3,
            },
          ],
          HitSymbol: [3, 4],
          HitAmount: [8, 9],
          Payoff: [1, 0.6],
        },
        {
          Columns: [
            {
              Symbol: [4, 4, 4, 8, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 4, 4, 0, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 5, 0, 4, 1],
              RemoveAmount: 0,
            },
            {
              Symbol: [3, 2, 100, 5, 0],
              RemoveAmount: 0,
            },
            {
              Symbol: [2, 0, 1, 0, 6],
              RemoveAmount: 0,
            },
            {
              Symbol: [0, 1, 4, 2, 0],
              RemoveAmount: 0,
            },
          ],
          HitSymbol: [],
          HitAmount: [],
          Payoff: [],
        },
      ],
    },
  ],
};

export const doubleCrash = {
  Event: "OnPlaceBet",
  ErrorCode: 0,
  ErrorMsg: "",
  Payload: {
    CustName: "twpgmsobichen",
    Balance: 118129.84,
    TotalPayoff: 0,
    Scatter: {
      Amount: 3,
      FreeTimes: 10,
    },
    Double: [],
    Cards: [
      {
        Columns: [
          {
            Symbol: [100, 5, 5, 7, 7],
            RemoveAmount: 0,
          },
          {
            Symbol: [100, 3, 7, 0, 8],
            RemoveAmount: 0,
          },
          {
            Symbol: [100, 4, 8, 4, 8],
            RemoveAmount: 0,
          },
          {
            Symbol: [4, 4, 5, 6, 3],
            RemoveAmount: 0,
          },
          {
            Symbol: [2, 7, 3, 4, 5],
            RemoveAmount: 0,
          },
          {
            Symbol: [3, 8, 0, 4, 7],
            RemoveAmount: 0,
          },
        ],
        HitSymbol: [],
        HitAmount: [],
        Payoff: [],
      },
    ],
    PromoActivity: {
      FreeRound: "0",
    },
    FreeGame: [
      {
        TotalPayOff: 1.6,
        Scatter: {
          Amount: 4,
          FreeTimes: 11,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [100, 3, 0, 3, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 4, 4, 3, 6],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 3, 3, 4, 4],
                RemoveAmount: 4,
              },
              {
                Symbol: [5, 4, 4, 2, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 3, 100, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 4, 100, 5, 2],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3, 4],
            HitAmount: [8, 8],
            Payoff: [1, 0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 0, 4, 100, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 4, 1, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 4, 0, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 2, 5, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 2, 100, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 4, 100, 5, 2],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 1.5,
        Scatter: {
          Amount: 3,
          FreeTimes: 11,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [6, 2, 100, 4, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 0, 6, 4, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 4, 4, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 1, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 3, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [7, 0, 8, 4, 1],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [11],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [0, 3, 6, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 0, 6, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 3, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 2, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 4, 2, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 7, 0, 8, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 6.5,
        Scatter: {
          Amount: 3,
          FreeTimes: 11,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 3, 4, 0, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [6, 2, 3, 5, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [7, 5, 3, 100, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 100, 3, 3, 6],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 0, 0, 4, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 100, 3, 1, 4],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [4, 4, 4, 0, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 6, 2, 5, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 4, 7, 5, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 3, 4, 100, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 0, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 0, 100, 1, 4],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 0, 0, 1],
                RemoveAmount: 4,
              },
              {
                Symbol: [4, 2, 6, 2, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 7, 5, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 1, 3, 100, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 4, 2, 0, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 1, 0, 100, 1],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [0],
            HitAmount: [8],
            Payoff: [4],
          },
          {
            Columns: [
              {
                Symbol: [4, 5, 0, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 6, 2, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 7, 5, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 1, 3, 100, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 4, 4, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 1, 100, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2.5,
        Scatter: {
          Amount: 4,
          FreeTimes: 12,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 2, 3, 0, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 100, 1, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 4, 4, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 1, 4, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 4, 4, 100, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 3, 5, 4],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [4, 0, 2, 3, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 100, 1, 2, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 4, 3, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [8, 2, 2, 1, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 7, 1, 100, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 100, 3, 3, 5],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [0, 4, 0, 2, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 3, 100, 1, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 2, 4, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 8, 2, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 7, 1, 100, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 1, 100, 5],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 1.6,
        Scatter: {
          Amount: 3,
          FreeTimes: 12,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [2, 3, 4, 1, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 3, 7, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [5, 2, 3, 7, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 2, 5, 4, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 0, 1, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 4, 6, 4, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [100, 4, 2, 3, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 3, 7, 2, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 5, 2, 3, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 100, 2, 5, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 3, 0, 1, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 0, 3, 6, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [5, 100, 4, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 1, 7, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 5, 2, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 0, 100, 2, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 2, 0, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 0, 6, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 11,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [6, 2, 2, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 0, 3, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 100, 8, 4, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 4, 0, 1, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 0, 3, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 3, 4, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [6, 2, 2, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 6, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 100, 8, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 0, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 2, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 5, 0, 3, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 6.6,
        Scatter: {
          Amount: 3,
          FreeTimes: 11,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 1, 1, 1, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 4, 0, 0, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 100, 6, 4, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 6, 4, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 4, 100, 3, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 3, 2, 4, 3],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 1, 1, 1, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 0, 0, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 0, 0, 100, 6],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 0, 0, 6, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [0, 0, 100, 3, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 3, 2, 3],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [0],
            HitAmount: [10],
            Payoff: [5],
          },
          {
            Columns: [
              {
                Symbol: [3, 1, 1, 1, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 0, 1, 2, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 4, 4, 100, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 3, 3, 6],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 3, 100, 3, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 3, 2, 3],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [6, 4, 1, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 0, 1, 2, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 4, 4, 100, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 2, 1, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 0, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 6, 100, 4, 2],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 4,
        Scatter: {
          Amount: 2,
          FreeTimes: 10,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 100, 3, 3, 3],
                RemoveAmount: 4,
              },
              {
                Symbol: [3, 4, 100, 4, 4],
                RemoveAmount: 4,
              },
              {
                Symbol: [3, 0, 5, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 1, 0, 4, 3],
                RemoveAmount: 3,
              },
              {
                Symbol: [7, 4, 4, 0, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 4, 0, 4],
                RemoveAmount: 4,
              },
            ],
            HitSymbol: [4, 3],
            HitAmount: [12, 8],
            Payoff: [3, 1],
          },
          {
            Columns: [
              {
                Symbol: [2, 4, 2, 3, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 4, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [8, 3, 4, 0, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 3, 1, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 7, 0, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 4, 2, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 1,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [8, 3, 3, 8, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 5, 0, 4, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 0, 4, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 3, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [5, 0, 1, 7, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [7, 4, 0, 1, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [8, 3, 3, 8, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 0, 5, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 4, 1, 0, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 100, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 5, 0, 1, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 7, 0, 1, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 15.5,
        Scatter: {
          Amount: 3,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 0, 4, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 4, 4, 4],
                RemoveAmount: 4,
              },
              {
                Symbol: [4, 4, 4, 4, 3],
                RemoveAmount: 4,
              },
              {
                Symbol: [2, 2, 4, 3, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 0, 6, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 3, 100, 2, 7],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [2, 0, 0, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 3, 0, 2, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [6, 8, 2, 2, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 2, 2, 3, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 0, 6, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 3, 100, 2, 7],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [2],
            HitAmount: [10],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 0, 0, 3],
                RemoveAmount: 5,
              },
              {
                Symbol: [0, 0, 3, 0, 1],
                RemoveAmount: 4,
              },
              {
                Symbol: [100, 4, 6, 8, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 0, 0, 3, 3],
                RemoveAmount: 5,
              },
              {
                Symbol: [6, 0, 6, 0, 3],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 5, 3, 100, 7],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [0, 3],
            HitAmount: [12, 8],
            Payoff: [10, 1],
          },
          {
            Columns: [
              {
                Symbol: [3, 4, 3, 0, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 1, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 100, 4, 6, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 1, 100, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 4, 6, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 5, 100, 7],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 15.1,
        Scatter: {
          Amount: 3,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 4, 1, 4, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 4, 3, 1, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 2, 3, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [6, 2, 2, 4, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 7, 4, 7, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 2, 100, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [4, 0, 4, 1, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [100, 4, 4, 1, 5],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 0, 2, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [8, 6, 2, 2, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 7, 4, 7],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 2, 2, 100, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [1, 2, 2, 0, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 4, 100, 1, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 0, 2, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 8, 6, 2, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 1, 7, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 2, 100, 6],
                RemoveAmount: 3,
              },
            ],
            HitSymbol: [2],
            HitAmount: [10],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [3, 0, 1, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 100, 1, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 4, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 4, 4, 8, 6],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 3, 1, 7, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 3, 4, 100, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 0, 1, 0, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 100, 1, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 1, 1, 2, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 0, 1, 8, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 3, 1, 7, 7],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 1, 3, 100, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [1],
            HitAmount: [9],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [5, 0, 3, 0, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 3, 3, 100, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 0, 100, 2, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 2, 0, 8, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 5, 3, 7, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 0, 3, 100, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [0],
            HitAmount: [8],
            Payoff: [4],
          },
          {
            Columns: [
              {
                Symbol: [4, 4, 1, 5, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 3, 3, 100, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 4, 2, 100, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 2, 2, 8, 6],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 5, 3, 7, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [7, 2, 3, 100, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [4, 4, 1, 5, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 3, 100, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 4, 4, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 1, 8, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 5, 3, 7, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 7, 3, 100, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2.5,
        Scatter: {
          Amount: 4,
          FreeTimes: 10,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 4, 3, 6, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 100, 2, 0, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 8, 3, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 7, 2, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 0, 3, 6],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 4, 4, 4, 3],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [0, 100, 0, 4, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [8, 4, 100, 2, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 100, 4, 8, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 1, 7, 2, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 100, 0, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 4, 4, 4],
                RemoveAmount: 4,
              },
            ],
            HitSymbol: [4],
            HitAmount: [11],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [6, 0, 100, 0, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 8, 100, 2, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 100, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 7, 2, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 100, 0, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 1, 4, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 3.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 3, 2, 1, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 2, 2, 4, 8],
                RemoveAmount: 4,
              },
              {
                Symbol: [100, 4, 2, 4, 2],
                RemoveAmount: 4,
              },
              {
                Symbol: [3, 3, 3, 4, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 4, 2, 2, 6],
                RemoveAmount: 4,
              },
              {
                Symbol: [2, 4, 2, 2, 2],
                RemoveAmount: 5,
              },
            ],
            HitSymbol: [2, 4],
            HitAmount: [11, 9],
            Payoff: [3, 0.6],
          },
          {
            Columns: [
              {
                Symbol: [2, 1, 3, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 100, 2, 6, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 5, 2, 4, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 3, 3, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 4, 4, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 2, 2, 4],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 9.2,
        Scatter: {
          Amount: 4,
          FreeTimes: 10,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [8, 8, 4, 3, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 3, 3, 1, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 1, 100, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 1, 0, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 1, 2, 4, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 0, 3, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [10],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [4, 8, 8, 4, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 4, 1, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 1, 1, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 1, 1, 0, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 1, 2, 4, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 2, 1, 0, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [1],
            HitAmount: [9],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [4, 8, 8, 4, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 100, 4, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 4, 2, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 4, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 2, 4, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 0, 2, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [2, 3, 8, 8, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 3, 3, 100, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 2, 2, 100],
                RemoveAmount: 3,
              },
              {
                Symbol: [8, 0, 3, 3, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 1, 3, 2, 2],
                RemoveAmount: 4,
              },
              {
                Symbol: [3, 2, 0, 2, 0],
                RemoveAmount: 3,
              },
            ],
            HitSymbol: [2, 3],
            HitAmount: [9, 8],
            Payoff: [2, 1],
          },
          {
            Columns: [
              {
                Symbol: [3, 1, 100, 8, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 4, 4, 100, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 1, 3, 4, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 4, 8, 0, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 4, 4, 4, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 4, 0, 0, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 1, 100, 8, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 1, 100, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 1, 3, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 8, 0, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 1, 100, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 0, 0, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 1.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 3, 0, 2, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [6, 3, 0, 8, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 2, 3, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 6, 1, 3, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 4, 4, 3, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [6, 2, 4, 100, 4],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [0, 4, 0, 2, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 7, 6, 0, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 7, 0, 2, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 4, 6, 1, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 3, 100, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [6, 2, 4, 100, 4],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 0, 2, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 7, 6, 0, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 7, 0, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 6, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 7, 4, 3, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 4, 6, 2, 100],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 3,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [7, 4, 100, 1, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 7, 4, 1, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [7, 2, 0, 7, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 2, 1, 4, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 4, 1, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 6, 4, 6],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 7, 100, 1, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 7, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 2, 0, 7, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 2, 1, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 100, 6, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 11.2,
        Scatter: {
          Amount: 3,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 2, 100, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [5, 2, 2, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 3, 2, 4, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 1, 1, 1, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [8, 2, 8, 4, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 7, 4, 8],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [1, 3, 100, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 5, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 4, 3, 4, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 4, 1, 1, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 8, 8, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 1, 7, 4, 8],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [1, 3, 100, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 3, 3, 5, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 100, 0, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 1, 1, 1],
                RemoveAmount: 4,
              },
              {
                Symbol: [1, 1, 8, 8, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 0, 1, 7, 8],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [1],
            HitAmount: [8],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [4, 3, 100, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 3, 3, 5, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 100, 0, 3, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 0, 7, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 6, 8, 8, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 0, 0, 7, 8],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [0],
            HitAmount: [8],
            Payoff: [4],
          },
          {
            Columns: [
              {
                Symbol: [3, 4, 3, 100, 3],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 3, 3, 5, 100],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 2, 100, 3, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 4, 4, 7, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 6, 8, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 7, 8],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [2, 0, 4, 4, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 4, 5, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 4, 1, 2, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 4, 4, 4, 7],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 2, 6, 8, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 7, 8],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [7, 8, 2, 0, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 3, 3, 5, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 1, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 2, 0, 0, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 6, 8, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 1, 7, 8],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 5.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 8,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [1, 3, 0, 2, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 8, 5, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 4, 4, 4, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [5, 0, 2, 4, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 3, 2, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 2, 100, 3, 3],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [1, 1, 3, 0, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 1, 8, 5, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 4, 5, 1, 1],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 1, 5, 0, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 2, 3, 2, 2],
                RemoveAmount: 4,
              },
              {
                Symbol: [0, 2, 100, 3, 3],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [1, 2],
            HitAmount: [8, 9],
            Payoff: [3, 2],
          },
          {
            Columns: [
              {
                Symbol: [0, 3, 3, 3, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 4, 8, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 1, 4, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 100, 5, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 7, 2, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 0, 100, 3, 3],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 0,
          FreeTimes: 7,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 4, 2, 7, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 4, 3, 8, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 2, 4, 0, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 2, 4, 8, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 2, 3, 1, 8],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 3, 0, 6, 4],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [1, 3, 2, 7, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [5, 1, 3, 8, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 3, 2, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 6, 2, 8, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 3, 1, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 0, 3, 0, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2.6,
        Scatter: {
          Amount: 5,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 0, 100, 4, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 4, 2, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 100, 2, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 0, 2, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 3, 3, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 0, 8, 4, 2],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [2, 0, 0, 100, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 1, 100, 4, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 1, 100, 4, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 0, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 1, 3, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 0, 8, 4],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [3, 2, 0, 0, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 3, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 1, 100, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 100, 0, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 3, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 2, 0, 0, 8],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 2,
          FreeTimes: 8,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 7, 3, 0, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 6, 4, 4, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 2, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 2, 100, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 2, 4, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 4, 2, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2.6,
        Scatter: {
          Amount: 4,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 3, 2, 100, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 1, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 1, 100, 2, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 2, 100, 4, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 4, 3, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 4, 4, 3, 0],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [0, 4, 3, 2, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 2, 1, 0, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 1, 100, 2, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 0, 2, 100, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 1, 2, 3, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 4, 1, 3, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [2],
            HitAmount: [9],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [2, 0, 4, 3, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 1, 100, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 6, 1, 0, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 100, 1, 1, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 1, 3, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 1,
          FreeTimes: 8,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [5, 2, 4, 3, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 7, 6, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 3, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 1, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 2, 4, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 4, 3, 2],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 7,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [2, 4, 4, 4, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 2, 0, 4, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 1, 7, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 0, 4, 4, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 100, 3, 3, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 5, 4, 0, 3],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [0, 1, 4, 2, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [5, 1, 2, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 0, 1, 7, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 0, 0, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 3, 3, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 5, 0, 3],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 8.5,
        Scatter: {
          Amount: 5,
          FreeTimes: 9,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [7, 4, 3, 3, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 4, 7, 100, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 3, 4, 100, 8],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 1, 3, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 3, 4, 1, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 4, 4, 2, 4],
                RemoveAmount: 4,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [0, 4, 7, 3, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 1, 7, 100, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 3, 100, 8],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 1, 3, 1, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 100, 7, 3, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 2, 3, 100, 2],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [7, 2, 0, 4, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 1, 7, 100, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 2, 100, 8],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 1, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 100, 7, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 2, 2, 100, 2],
                RemoveAmount: 3,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [2, 7, 0, 4, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 7, 100, 7],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 0, 4, 100, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 1, 1, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 3, 100, 7, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 3, 1, 4, 100],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [1],
            HitAmount: [8],
            Payoff: [3],
          },
          {
            Columns: [
              {
                Symbol: [2, 7, 0, 4, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 7, 100, 7],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 0, 4, 100, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 3, 3, 100],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 3, 3, 100, 7],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 3, 4, 100],
                RemoveAmount: 3,
              },
            ],
            HitSymbol: [3],
            HitAmount: [9],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [2, 7, 0, 4, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 7, 100, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 4, 100, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 4, 4, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 3, 2, 100, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 0, 6, 4, 100],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 2,
          FreeTimes: 8,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [5, 100, 1, 1, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [8, 4, 0, 5, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 5, 4, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 0, 2, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 2, 1, 4, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 0, 2, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 1,
          FreeTimes: 7,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 7, 6, 2, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 4, 4, 7, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [0, 4, 4, 1, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [1, 2, 2, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 100, 2, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 5, 3, 4, 1],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [1, 7, 6, 2, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [7, 3, 6, 2, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 3, 0, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 2, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 100, 2, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 5, 3, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 3.5,
        Scatter: {
          Amount: 2,
          FreeTimes: 6,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 4, 2, 3, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 3, 4, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 2, 4, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 4, 0, 4, 6],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 3, 1, 100, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 5, 4, 5, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [10],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [1, 4, 2, 3, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 2, 3, 3, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 2, 0, 2, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [5, 2, 1, 0, 6],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 2, 3, 1, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 0, 5, 5, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [2, 1, 4, 3, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 0, 3, 3, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 0, 1, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 5, 1, 0, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 3, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 5, 5, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0.6,
        Scatter: {
          Amount: 3,
          FreeTimes: 6,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [6, 6, 4, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [0, 2, 3, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 2, 0, 6, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 4, 3, 1, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 2, 7, 1, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 3, 2, 4],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [2, 1, 3, 6, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 2, 3, 2, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 2, 0, 6, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 100, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 5, 2, 7, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 2, 3, 2],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 3,
          FreeTimes: 6,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [7, 4, 7, 2, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 1, 3, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 1, 1, 2, 6],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 100, 2, 3, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 3, 0, 1, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 2, 4, 3, 2],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 7.5,
        Scatter: {
          Amount: 3,
          FreeTimes: 6,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [4, 1, 4, 4, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [100, 2, 3, 4, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 4, 4, 4, 4],
                RemoveAmount: 4,
              },
              {
                Symbol: [4, 3, 2, 0, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 7, 4, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 0, 3, 1, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [11],
            Payoff: [1.5],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 3, 1, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 100, 2, 3, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 1, 0, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 4, 3, 2, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 3, 2, 7, 100],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 0, 3, 1, 6],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [0],
            HitAmount: [8],
            Payoff: [4],
          },
          {
            Columns: [
              {
                Symbol: [4, 5, 3, 1, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 100, 2, 3, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 3, 4, 1, 3],
                RemoveAmount: 3,
              },
              {
                Symbol: [3, 1, 4, 3, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 3, 2, 7, 100],
                RemoveAmount: 2,
              },
              {
                Symbol: [0, 3, 3, 1, 6],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [11],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [4, 4, 5, 1, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 100, 2, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 100, 4, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 1, 4, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 1, 2, 7, 100],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 7, 0, 1, 6],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2.6,
        Scatter: {
          Amount: 3,
          FreeTimes: 6,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 4, 4, 2, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 1, 1, 2, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 3, 0, 3, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 5, 7, 3, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 4, 3, 5, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 3, 0, 4, 1],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [4, 3, 4, 4, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 1, 1, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 3, 3, 0, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 2, 5, 7, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 4, 5, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 4, 0, 4, 1],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [4, 3, 3, 3, 2],
                RemoveAmount: 3,
              },
              {
                Symbol: [100, 5, 1, 1, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 3, 0, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 2, 5, 7, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 3, 100, 5, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [4, 3, 0, 0, 1],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [100, 3, 6, 4, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 5, 1, 1, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 6, 4, 0, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 2, 5, 7, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 100, 5, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 0, 0, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 2,
          FreeTimes: 5,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [3, 2, 4, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 4, 4, 4, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 3, 1, 8],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 7, 0, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 100, 4, 1, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [0, 1, 3, 7, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 2,
        Scatter: {
          Amount: 2,
          FreeTimes: 4,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [1, 3, 3, 2, 0],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 2, 3, 2, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 4, 8, 1, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 4, 6, 4, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [100, 2, 3, 1, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [100, 4, 1, 4, 4],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [4, 1, 3, 3, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 1, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [5, 0, 4, 8, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 4, 6, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 2, 100, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [100, 4, 1, 4, 4],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 8.4,
        Scatter: {
          Amount: 2,
          FreeTimes: 3,
        },
        Double: [2],
        Cards: [
          {
            Columns: [
              {
                Symbol: [100, 1, 4, 4, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 100, 92, 7, 3],
                RemoveAmount: 1,
              },
              {
                Symbol: [1, 8, 4, 2, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 3, 4, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 5, 4, 3, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [5, 3, 0, 2, 3],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [4],
            HitAmount: [9],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [2, 2, 100, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 100, 92, 7, 3],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 1, 8, 2, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 3, 3, 3],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 1, 5, 3, 2],
                RemoveAmount: 1,
              },
              {
                Symbol: [5, 3, 0, 2, 3],
                RemoveAmount: 2,
              },
            ],
            HitSymbol: [3],
            HitAmount: [8],
            Payoff: [1],
          },
          {
            Columns: [
              {
                Symbol: [2, 2, 100, 1, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 4, 100, 92, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 8, 2, 1],
                RemoveAmount: 2,
              },
              {
                Symbol: [2, 3, 4, 4, 4],
                RemoveAmount: 1,
              },
              {
                Symbol: [3, 2, 1, 5, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [7, 4, 5, 0, 2],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [2],
            HitAmount: [8],
            Payoff: [2],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 100, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 100, 92, 7],
                RemoveAmount: 2,
              },
              {
                Symbol: [4, 0, 1, 8, 1],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 3, 4, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [4, 0, 3, 1, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [2, 7, 4, 5, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [0, 0, 100, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 100, 92, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 0, 1, 8, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 5, 2, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 0, 3, 1, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 7, 5, 0],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 4.6,
        Scatter: {
          Amount: 2,
          FreeTimes: 2,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 3, 4, 2, 4],
                RemoveAmount: 2,
              },
              {
                Symbol: [1, 1, 4, 4, 4],
                RemoveAmount: 3,
              },
              {
                Symbol: [2, 1, 3, 0, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 3, 0, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [6, 100, 2, 0, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 1, 4, 100, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [4],
            HitAmount: [8],
            Payoff: [0.6],
          },
          {
            Columns: [
              {
                Symbol: [0, 5, 0, 3, 2],
                RemoveAmount: 2,
              },
              {
                Symbol: [3, 4, 6, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 1, 3, 0, 5],
                RemoveAmount: 1,
              },
              {
                Symbol: [0, 1, 3, 0, 0],
                RemoveAmount: 3,
              },
              {
                Symbol: [6, 100, 2, 0, 0],
                RemoveAmount: 2,
              },
              {
                Symbol: [5, 1, 1, 100, 0],
                RemoveAmount: 1,
              },
            ],
            HitSymbol: [0],
            HitAmount: [9],
            Payoff: [4],
          },
          {
            Columns: [
              {
                Symbol: [3, 5, 5, 3, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 4, 6, 1, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 2, 1, 3, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [6, 4, 0, 1, 3],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 4, 6, 100, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 5, 1, 1, 100],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 1,
          FreeTimes: 1,
        },
        Double: [5],
        Cards: [
          {
            Columns: [
              {
                Symbol: [0, 4, 1, 0, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 8, 2, 3, 7],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 2, 1, 2, 4],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 4, 0, 100, 0],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 1, 3, 3, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 7, 6, 95, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
      {
        TotalPayOff: 0,
        Scatter: {
          Amount: 1,
          FreeTimes: 0,
        },
        Double: [],
        Cards: [
          {
            Columns: [
              {
                Symbol: [2, 4, 4, 4, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [2, 100, 4, 2, 5],
                RemoveAmount: 0,
              },
              {
                Symbol: [1, 2, 1, 0, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [4, 3, 0, 5, 1],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 0, 3, 4, 2],
                RemoveAmount: 0,
              },
              {
                Symbol: [3, 2, 1, 4, 1],
                RemoveAmount: 0,
              },
            ],
            HitSymbol: [],
            HitAmount: [],
            Payoff: [],
          },
        ],
      },
    ],
  },
};

export const manyCrashInfo = {
  TotalPayOff: 13.8,
  Scatter: {
    Amount: 1,
    FreeTimes: 5,
  },
  Double: [],
  Cards: [
    {
      Columns: [
        {
          Symbol: [4, 3, 3, 4, 1],
          RemoveAmount: 4,
        },
        {
          Symbol: [0, 3, 2, 1, 4],
          RemoveAmount: 2,
        },
        {
          Symbol: [3, 1, 3, 4, 3],
          RemoveAmount: 4,
        },
        {
          Symbol: [0, 2, 4, 4, 1],
          RemoveAmount: 2,
        },
        {
          Symbol: [2, 0, 3, 4, 2],
          RemoveAmount: 2,
        },
        {
          Symbol: [4, 3, 3, 4, 2],
          RemoveAmount: 4,
        },
      ],
      HitSymbol: [4, 3],
      HitAmount: [9, 9],
      Payoff: [0.6, 1],
    },
    {
      Columns: [
        {
          Symbol: [0, 5, 0, 2, 1],
          RemoveAmount: 1,
        },
        {
          Symbol: [3, 3, 0, 2, 1],
          RemoveAmount: 1,
        },
        {
          Symbol: [0, 4, 1, 5, 1],
          RemoveAmount: 0,
        },
        {
          Symbol: [4, 2, 0, 2, 1],
          RemoveAmount: 2,
        },
        {
          Symbol: [0, 3, 2, 0, 2],
          RemoveAmount: 2,
        },
        {
          Symbol: [2, 3, 1, 1, 2],
          RemoveAmount: 2,
        },
      ],
      HitSymbol: [2],
      HitAmount: [8],
      Payoff: [2],
    },
    {
      Columns: [
        {
          Symbol: [4, 0, 5, 0, 1],
          RemoveAmount: 2,
        },
        {
          Symbol: [0, 3, 3, 0, 1],
          RemoveAmount: 2,
        },
        {
          Symbol: [0, 4, 1, 5, 1],
          RemoveAmount: 1,
        },
        {
          Symbol: [4, 0, 4, 0, 1],
          RemoveAmount: 2,
        },
        {
          Symbol: [4, 3, 0, 3, 0],
          RemoveAmount: 2,
        },
        {
          Symbol: [0, 4, 3, 1, 1],
          RemoveAmount: 1,
        },
      ],
      HitSymbol: [0],
      HitAmount: [10],
      Payoff: [5],
    },
    {
      Columns: [
        {
          Symbol: [3, 2, 4, 5, 1],
          RemoveAmount: 3,
        },
        {
          Symbol: [3, 3, 3, 3, 1],
          RemoveAmount: 5,
        },
        {
          Symbol: [1, 4, 1, 5, 1],
          RemoveAmount: 4,
        },
        {
          Symbol: [0, 4, 4, 4, 1],
          RemoveAmount: 4,
        },
        {
          Symbol: [4, 1, 4, 3, 3],
          RemoveAmount: 5,
        },
        {
          Symbol: [4, 4, 3, 1, 1],
          RemoveAmount: 5,
        },
      ],
      HitSymbol: [3, 4, 1],
      HitAmount: [8, 9, 9],
      Payoff: [1, 0.6, 3],
    },
    {
      Columns: [
        {
          Symbol: [8, 4, 7, 2, 5],
          RemoveAmount: 1,
        },
        {
          Symbol: [3, 0, 4, 3, 3],
          RemoveAmount: 1,
        },
        {
          Symbol: [2, 2, 4, 0, 5],
          RemoveAmount: 1,
        },
        {
          Symbol: [2, 6, 1, 4, 0],
          RemoveAmount: 1,
        },
        {
          Symbol: [4, 4, 2, 1, 3],
          RemoveAmount: 2,
        },
        {
          Symbol: [4, 100, 4, 5, 0],
          RemoveAmount: 2,
        },
      ],
      HitSymbol: [4],
      HitAmount: [8],
      Payoff: [0.6],
    },
    {
      Columns: [
        {
          Symbol: [0, 8, 7, 2, 5],
          RemoveAmount: 0,
        },
        {
          Symbol: [7, 3, 0, 3, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [1, 2, 2, 0, 5],
          RemoveAmount: 0,
        },
        {
          Symbol: [8, 2, 6, 1, 0],
          RemoveAmount: 0,
        },
        {
          Symbol: [4, 1, 2, 1, 3],
          RemoveAmount: 0,
        },
        {
          Symbol: [2, 0, 100, 5, 0],
          RemoveAmount: 0,
        },
      ],
      HitSymbol: [],
      HitAmount: [],
      Payoff: [],
    },
  ],
};
