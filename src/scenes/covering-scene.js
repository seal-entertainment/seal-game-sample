import { EVENT_NAME } from "../constants/events";
import { FreeGamePopup } from "../objects/popups/free-game-banner";
import { MessagePopup } from "../objects/popups/message-popup";

const config = {
  covering: {
    style: { fillStyle: { color: 0x000000, alpha: 0.6 } },
  },
  text: {
    x: 340,
    y: 350,
    style: {
      fontSize: "40px",
      fontFamily: "Helvetica",
      strokeThickness: 1,
      color: "#FF0000",
      wordWrap: { width: 600 },
    },
  },
  popup: {
    freeGame: { x: 334, y: 400 },
    message: { x: 340, y: 350 },
  },
};

export default class CoveringScene extends Phaser.Scene {
  constructor() {
    super("CoveringScene");
  }

  handleResize() {
    if (window.innerWidth / window.innerHeight < 0.5625 && window.innerWidth < this.game.canvas.clientWidth) {
      const ratio = window.innerWidth / (this.game.canvas.clientWidth * 0.5625);
      this.cameras.main.setZoom(ratio);
    } else {
      this.cameras.main.setZoom(1);
    }
  }

  create() {
    window.addEventListener("resize", () => this.handleResize(), false);
    this.drawCovering();
    this.freeGamePopup = new FreeGamePopup(this, config.popup.freeGame.x, config.popup.freeGame.y).setVisible(false);
    this.messagePopup = new MessagePopup(this, config.popup.message.x, config.popup.message.y).setVisible(false);
    this.game.events.on(EVENT_NAME.SHOW_ERROR, (evt) => this.showError(evt));
    this.game.events.on(EVENT_NAME.ENTER_FREE, ([isShow, updateValue]) => {
      this.covering.setVisible(isShow);
      if (isShow) this.freeGamePopup.showFreeTimes(updateValue);
      else this.freeGamePopup.showTotalPayoff(updateValue);
    });
    this.freeGamePopup.on(EVENT_NAME.CLOSE_FREE_GAME_POPUP, () => {
      this.game.events.emit(EVENT_NAME.CLOSE_FREE_GAME_POPUP);
      this.covering.setVisible(false);
    });
  }

  drawCovering() {
    this.covering = this.add.graphics(config.covering.style);
    this.covering.fillRect(0, 0, this.game.canvas.width, this.game.canvas.height);
    this.covering.setVisible(false);
  }

  showError(evt) {
    // console.log("show Error", evt);
    this.add.text(config.text.x, config.text.y, `[${evt.ErrorCode}] ${evt.ErrorMsg}`, config.text.style);
  }

  showMessage(evt) {
    // console.log("show Message", evt);
    this.messagePopup.setMessage(`[${evt.ErrorCode}] ${evt.ErrorMsg}`);
    this.messagePopup.setVisible(true);
  }
}
