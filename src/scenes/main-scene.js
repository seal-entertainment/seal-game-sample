import Decimal from "decimal.js";
import { EVENT_NAME } from "../constants/events";
import { changeImageByState } from "../utils";
import { CascadingController } from "../objects/cascading-controller";
import { SpriteSheetAnimation } from "../objects/animation/sprite-sheet-animation";
import { Footer } from "../objects/footer";
import { CrashInfoFrame } from "../objects/crash-info/crash-info-frame";
import { BetControlPopup } from "../objects/popups/bet-control-popup";
import { SettingsPopup } from "../objects/popups/settings-popup";
import { AutoplayPopup } from "../objects/popups/autoplay-popup";
import { BasicImageButton } from "../objects/buttons/basic-image-button";
import { BuyFreeButton } from "../objects/buttons/buy-free-button";
import { SpinButton } from "../objects/buttons/spin-button";
import { FreeTimesFrame } from "../objects/free-times-frame";
import { FreeGameDouble } from "../objects/free-game-double";
import { FreeGameScore } from "../objects/free-game-score";
import { TumbleScore } from "../objects/tumble-score";
import { RulesPopup } from "../min.js/rules.min.js";
import { Digits } from "../objects/digits";
import { DoubleChanceButton } from "../objects/buttons/double-chance-button";

const config = {
  defaultBet: "1.00",
  betList: [
    "0.20",
    "1.00",
    "2.00",
    "3.00",
    "4.00",
    "5.00",
    "6.00",
    "7.00",
    "8.00",
    "9.00",
    "10.00",
    "12.00",
    "14.00",
    "16.00",
    "20.00",
  ],
  dashboard: {
    x: 6,
    y: 249,
  },
  cascading: {
    x: 96,
    y: 215,
  },
  mainCharacter: {
    x: 550,
    y: 263,
    minScale: 1,
    maxScale: 1.1,
    updateScale: 0.001,
    delay: 800,
    spriteSheet: {
      texture: "atlas_2",
      key: "artillery",
      frameNames: [
        {
          texture: "atlas_2",
          prefix: "artillery/",
          zeroPad: 2,
          frames: [1, 1, 2, 2, 3, 4, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 8, 8, 1],
        },
      ],
      repeat: 0,
    },
  },
  logo: {
    x: -2,
    y: 163,
  },
  explosion: {
    x: 600,
    y: 140,
    delay: 400,
    spriteSheet: {
      texture: "atlas_3",
      key: "explosion",
      frameNames: [
        {
          texture: "atlas_3",
          prefix: "explosion/",
          end: 16,
          zeroPad: 2,
        },
      ],
      repeat: 0,
    },
  },
  footer: {
    style: {
      fillStyle: { color: 0x555555, alpha: 1 },
      lineStyle: { color: 0xffffff },
    },
    x: 0,
    y: 1209,
    width: 720,
    height: 70,
  },
  btn: {
    doubleChance: {
      x: 261,
      y: 835,
    },
    rules: {
      x: 65,
      y: 985,
    },
    buyFree: {
      x: 41,
      y: 839,
    },
    coin: {
      x: 370,
      y: 1135,
    },
    auto: {
      x: 370,
      y: 995,
    },
    spin: {
      x: 213,
      y: 1072,
    },
    settings: {
      x: 65,
      y: 1215,
    },
  },
  popups: {
    x: 60,
    y: {
      s: 350,
      m: 250,
      l: 100,
    },
  },
  crashInfo: {
    x: 448,
    y: 940,
    freeMode: {
      x: 250,
      y: 880,
    },
  },
  tumbleScore: {
    x: 22,
    y: 109,
  },
  freeGameDelay: 1000,
  freeTimesFrame: { x: 40, y: 930 },
  freeGameDouble: { x: 493, y: 957 },
  freeGameScore: { x: 80, y: 791 },
  slogan: {
    x: 212.5,
    y: 791,
    style: {
      font: "700 30px Helvetica",
      lineHeight: "34px",
      color: "#FFFFFF",
    },
  },
  flyingDouble: {
    base: {
      x: 360,
      y: 520,
    },
    free: {
      x: 593,
      y: 997,
    },
    stringSpacing: 0,
    digitSpacing: 0,
    scale: 1.8,
  },
  tween: {
    double: {
      props: {
        x: { value: 260, duration: 700, delay: 300, ease: "Sine.easeIn" },
        y: { value: 130, duration: 700, delay: 300 },
        scaleX: { value: 0, duration: 1000, ease: "Back.easeIn", easeParams: [10] },
        scaleY: { value: 0, duration: 1000, ease: "Back.easeIn", easeParams: [10] },
      },
    },
  },
};
export default class MainScene extends Phaser.Scene {
  constructor() {
    super("MainScene");
    this.gameInfo = null;
    this.nowBet = config.defaultBet;
    this.betList = config.betList;
    this.isTurbo = false;
    this.isAuto = false;
    this.autoTimes = 0;
  }

  handleResize() {
    this.cameras.main.setScroll((720 - 1280) / 2, 0);
    if (window.innerWidth / window.innerHeight < 0.5625 && window.innerWidth < this.game.canvas.clientWidth) {
      const ratio = window.innerWidth / (this.game.canvas.clientWidth * 0.5625);
      this.cameras.main.setZoom(ratio);
    } else {
      this.cameras.main.setZoom(1);
    }
  }

  init(data) {
    this.gameData = data;
    this.isDoubleChanceButtonOn = false;
    this.isArtilleryMax = false;
  }

  create() {
    // console.log("enter MainScene", this, this.gameData);
    this.handleResize();
    window.addEventListener("resize", () => this.handleResize(), false);
    if (parseInt(this.nowBet) < this.gameData.MinBet) this.nowBet = this.gameData.MinBet.toString();
    this.game.events.on(EVENT_NAME.ON_BUY_FREE_GAME, (gameInfo) => this.onPlaceBet(gameInfo));
    this.game.events.on(EVENT_NAME.ON_PLACE_BET, (gameInfo) => this.onPlaceBet(gameInfo));
    this.game.events.on(EVENT_NAME.ON_GET_BALANCE, (info) => this.updateBalance(info.Balance.toString()));
    this.game.events.on(EVENT_NAME.UPDATE_VALUE, (delta) => this.updateBet(delta));

    this.add.image(config.dashboard.x, config.dashboard.y, "atlas_1", "dashboard").setOrigin(0);
    this.cascading = new CascadingController(this, config.cascading.x, config.cascading.y);
    this.slogan = this.add.text(config.slogan.x, config.slogan.y, "PLACE YOUR BETS!", config.slogan.style);
    this.tumbleScore = new TumbleScore(this, config.tumbleScore.x, config.tumbleScore.y);
    this.createHeader();
    this.createFooter();
    this.createButtons();
    this.freeGameScore = new FreeGameScore(this, config.freeGameScore.x, config.freeGameScore.y);
    this.freeGameDouble = new FreeGameDouble(this, config.freeGameDouble.x, config.freeGameDouble.y);
    this.freeTimesFrame = new FreeTimesFrame(this, config.freeTimesFrame.x, config.freeTimesFrame.y);
    this.crashInfos = new CrashInfoFrame(this, config.crashInfo.x, config.crashInfo.y);
    this.createPopups();
    this.updateBet();
  }

  update() {
    if (!this.mainCharacter.anims.isPlaying) {
      if (this.isArtilleryMax) {
        this.mainCharacter.setScale(this.mainCharacter.scale - config.mainCharacter.updateScale);
        if (this.mainCharacter.scale <= config.mainCharacter.minScale) this.isArtilleryMax = false;
      } else {
        this.mainCharacter.setScale(this.mainCharacter.scale + config.mainCharacter.updateScale);
        if (this.mainCharacter.scale >= config.mainCharacter.maxScale) this.isArtilleryMax = true;
      }
    }
  }

  createHeader() {
    this.mainCharacter = new SpriteSheetAnimation(
      this,
      config.mainCharacter.x,
      config.mainCharacter.y,
      config.mainCharacter.spriteSheet
    );
    this.mainCharacter.setOrigin(0.5, 1);
    this.add.image(config.logo.x, config.logo.y, "atlas_1", "header").setOrigin(0);
    this.explosion = new SpriteSheetAnimation(
      this,
      config.explosion.x,
      config.explosion.y,
      config.explosion.spriteSheet
    );
  }

  createFooter() {
    const footerGraphic = this.add.graphics(config.footer.style);
    footerGraphic.fillRect(config.footer.x, config.footer.y, config.footer.width, config.footer.height);
    footerGraphic.strokeRect(config.footer.x, config.footer.y, config.footer.width, config.footer.height);
    this.footer = new Footer(this, config.footer.x, config.footer.y);
    this.footer.updateBalance(this.gameData.Balance.toString());
    this.footer.updateBet(this.nowBet);
  }

  createButtons() {
    this.btnSpin = new SpinButton(this, config.btn.spin.x, config.btn.spin.y);
    this.btnAuto = new BasicImageButton(this, config.btn.auto.x, config.btn.auto.y, "atlas_1", "auto_up");
    this.btnCoin = new BasicImageButton(this, config.btn.coin.x, config.btn.coin.y, "atlas_1", "coin_up");
    this.btnSettings = new BasicImageButton(
      this,
      config.btn.settings.x,
      config.btn.settings.y,
      "atlas_1",
      "setting_up"
    );
    this.btnRules = new BasicImageButton(this, config.btn.rules.x, config.btn.rules.y, "atlas_1", "rules_up");
    this.btnBuyFree = new BuyFreeButton(this, config.btn.buyFree.x, config.btn.buyFree.y);
    this.btnDoubleChance = new DoubleChanceButton(this, config.btn.doubleChance.x, config.btn.doubleChance.y);
    this.btnDoubleChance.onButtonUp(() => {
      this.isDoubleChanceButtonOn = !this.isDoubleChanceButtonOn;
      this.updateBet();
    });
    this.btnBuyFree.onButtonUp(() => this.spin(true));
    this.btnSpin.onButtonUp(() => this.spin());
    this.btnSpin.on(EVENT_NAME.STOP_AUTO, () => (this.isAuto = false));
    this.btnRules.onButtonUp = () => this.openPopup(this.btnRules, this.rulesPanel);
    this.btnAuto.onButtonUp = () => this.openPopup(this.btnAuto, this.autoplaySetting);
    this.btnCoin.onButtonUp = () => this.openPopup(this.btnCoin, this.betPanel);
    this.btnSettings.onButtonUp = () => this.openPopup(this.btnSettings, this.settingsPanel);
  }

  createPopups() {
    this.rulesPanel = new RulesPopup(this, config.popups.x, config.popups.y.l, this.btnRules, this.gameData);
    this.betPanel = new BetControlPopup(this, config.popups.x, config.popups.y.s, this.btnCoin);
    this.settingsPanel = new SettingsPopup(this, config.popups.x, config.popups.y.m, this.btnSettings);
    this.autoplaySetting = new AutoplayPopup(this, config.popups.x, config.popups.y.m, this.btnAuto);
    this.autoplaySetting.on(EVENT_NAME.TURBO_CHANGED, (boolean) => (this.isTurbo = boolean));
    this.autoplaySetting.on(EVENT_NAME.AUTOPLAY, (times) => {
      this.isAuto = true;
      this.btnSpin.updateAutoTimes(times);
      this.autoTimes = times;
      this.checkAuto();
    });
  }

  openPopup(btn, popup) {
    if (!btn.isClick) this.events.emit(EVENT_NAME.CLOSE_POPUP);
    btn.isClick = !btn.isClick;
    changeImageByState(btn, btn.isClick ? "down" : "up");
    popup.setVisible(btn.isClick);
  }

  updateBet(delta = 0) {
    let index = this.betList.indexOf(this.nowBet);
    index += delta;
    if (index < 0) index = 0;
    if (index >= this.betList.length) index = this.betList.length - 1;
    this.nowBet = this.betList[index];
    if (this.isDoubleChanceButtonOn) this.footer.updateBet(Decimal.mul(this.nowBet, 1.25).toFixed(2));
    else this.footer.updateBet(this.nowBet);
    this.btnDoubleChance.updatePrice(Decimal.mul(this.nowBet, 1.25).toFixed(2));
    this.btnBuyFree.updatePrice(Decimal.mul(this.nowBet, 100).toFixed(2));
    this.game.events.emit(EVENT_NAME.UPDATE_BET, this.nowBet);
  }

  spin(isBuyFree = false) {
    this.events.emit(EVENT_NAME.CLOSE_POPUP);
    this.btnSpin.lock(true);
    this.btnBuyFree.lock(true);
    this.btnDoubleChance.lock(true);
    this.placeBet(isBuyFree);
    this.crashInfos.clearInfoList();
  }

  artilleryFire() {
    this.mainCharacter.play(config.mainCharacter.spriteSheet.key);
    this.mainCharacter.setScale(config.mainCharacter.minScale);
    this.isArtilleryMax = false;
    this.time.delayedCall(config.explosion.delay, () => {
      this.explosion.play(config.explosion.spriteSheet.key);
      this.sound.get("s_bomb").play();
    });
  }

  placeBet(isBuyFree) {
    const balance = this.gameInfo ? this.gameInfo.Balance : this.gameData.Balance;
    const stake = Decimal.mul(this.nowBet, isBuyFree ? 100 : 1).toFixed(2);
    this.nowBalance = Decimal.sub(balance, stake).toFixed(2);
    if (isBuyFree) this.game.events.emit(EVENT_NAME.BUY_FREE_GAME, parseInt(this.nowBet));
    else this.game.events.emit(EVENT_NAME.PLACE_BET, [parseInt(this.nowBet), this.isDoubleChanceButtonOn]);
  }

  onPlaceBet(gameInfo) {
    this.footer.updateBalance(this.nowBalance);
    this.gameInfo = JSON.parse(JSON.stringify(gameInfo));
    this.cascading.once(EVENT_NAME.CARDS_READY, () => this.checkWinLose());
    this.cascading.fallInCards(gameInfo.Cards[0].Columns, this.isTurbo); // 新局落下表演
    if (gameInfo.Double.length > 0) this.time.delayedCall(config.mainCharacter.delay, () => this.artilleryFire());
  }

  // 是否有贏分
  checkWinLose() {
    // console.log("checkWinLose:", this.gameInfo.TotalPayoff);
    if (this.gameInfo.Cards.length > 1) {
      //後端scatter還沒做好 先暫時加判斷
      this.cascading.once(EVENT_NAME.ANIMATION_COMPLETED, (nowWin) => this.checkDouble(nowWin));
      this.cascading.playWinAnimation(this.gameInfo.Cards);
    } else {
      this.checkScatter();
    }
    // this.cascading.reels[0].symbols[0].crash();
  }

  // 是否觸發乘倍符號
  checkDouble(nowWin) {
    // console.log("checkDouble:", this.gameInfo.Double, nowWin, this.gameInfo.TotalPayoff);
    if (this.gameInfo.Double.length > 0) {
      this.cascading.once(EVENT_NAME.DOUBLE_COMPLETED, (double) => {
        this.createFlyingDouble(double);
        if (this.isFreeGame && this.freeDouble) this.createFreeGameFlyingDouble();
      });
      this.cascading.winDouble(this.gameInfo.Double); //顯示分數乘倍動畫
    } else {
      this.checkFullScreenAnimation(nowWin);
    }
  }

  createFlyingDouble(double) {
    const totalDouble = new Digits(this, config.flyingDouble.base.x, config.flyingDouble.base.y, "atlas_1", "double/");
    totalDouble.setCharacter(12, "x");
    totalDouble.setScale(config.flyingDouble.scale);
    totalDouble.setStringSpacing(config.flyingDouble.stringSpacing);
    totalDouble.setDigitSpacing(config.flyingDouble.digitSpacing);
    totalDouble.displayWithString(double + "x");
    let tweenConfig = config.tween.double;
    tweenConfig.targets = totalDouble;
    tweenConfig.onComplete = (tween) => {
      this.tweens.remove(tween);
      totalDouble.destroy();
      this.events.emit(EVENT_NAME.UPDATE_VALUE, this.gameInfo.TotalPayoff);
      this.checkFullScreenAnimation();
      if (this.isFreeGame) {
        this.freeDouble += double;
        this.freeGameDouble.updateDouble(this.freeDouble);
      }
    };
    this.tweens.add(config.tween.double);
  }

  createFreeGameFlyingDouble() {
    const freeDouble = new Digits(this, config.flyingDouble.free.x, config.flyingDouble.free.y, "atlas_1", "double/");
    freeDouble.setCharacter(12, "x");
    freeDouble.setScale(config.flyingDouble.scale);
    freeDouble.setStringSpacing(config.flyingDouble.stringSpacing);
    freeDouble.setDigitSpacing(config.flyingDouble.digitSpacing);
    freeDouble.displayWithString(this.freeDouble + "x");
    let tweenConfig = config.tween.double;
    tweenConfig.targets = freeDouble;
    tweenConfig.onComplete = (tween) => {
      this.tweens.remove(tween);
      freeDouble.destroy();
    };
    this.tweens.add(config.tween.double);
  }

  // 金額是否達到大獎
  checkFullScreenAnimation(totalPayoff) {
    this.freeGameScore.updateScore(this.freeGameTotalPayoff);
    // console.log("checkFullScreenAnimation", totalPayoff, ">=", Decimal.mul(this.nowBet, 10).toNumber());
    if (totalPayoff >= Decimal.mul(this.nowBet, 10).toNumber()) {
      // console.log("play full screen animation");
      this.checkScatter();
    } else {
      this.checkScatter();
    }
  }

  // 是否觸發免費遊戲
  checkScatter() {
    // console.log("checkScatter", this.gameInfo.Scatter);
    if (this.gameInfo.Scatter.FreeTimes && this.gameInfo.Scatter.Amount > 2) {
      // console.log("play Scatter animation");
      this.freeTimes = this.gameInfo.Scatter.FreeTimes;
      this.cascading.winScatter();
      this.time.delayedCall(config.freeGameDelay * 2, () => {
        if (this.isFreeGame) {
          this.freeTimesFrame.updateFreeTimes(this.freeTimes);
          this.updateBalance(Decimal.add(this.nowBalance, this.gameInfo.TotalPayoff).toFixed(2));
        } else this.enterFreeGame();
      });
    } else {
      this.updateBalance(
        this.isFreeGame
          ? Decimal.add(this.nowBalance, this.gameInfo.TotalPayoff).toFixed(2)
          : this.gameInfo.Balance.toString()
      );
    }
  }

  updateBalance(str) {
    // console.log("show balance", str);
    this.nowBalance = str;
    this.footer.updateBalance(str);
    this.tumbleScore.close();
    if (!this.isFreeGame) this.checkAuto();
    else if (this.freeTimes > 0) this.time.delayedCall(config.freeGameDelay, () => this.spinFreeGame());
    else this.endFreeGame();
  }

  checkAuto() {
    this.isAuto = this.isAuto ? this.autoTimes > 0 : false;
    // console.log("checkAuto", this.isAuto, this.autoTimes);
    this.btnAuto.setVisible(!this.isAuto);
    this.btnCoin.setVisible(!this.isAuto);
    this.btnRules.setVisible(!this.isAuto);
    this.btnBuyFree.lock(this.isAuto && this.isDoubleChanceButtonOn);
    this.btnDoubleChance.lock(this.isAuto);
    if (this.isAuto) this.autoSpin();
    else {
      this.autoTimes = 0;
      this.btnSpin.closeAuto();
      this.btnSpin.lock(false);
    }
  }

  autoSpin() {
    this.spin();
    this.autoTimes--;
    this.btnSpin.updateAutoTimes(this.autoTimes);
  }

  // ---------- 免費遊戲 --------------
  enterFreeGame() {
    this.game.events.once(EVENT_NAME.CLOSE_FREE_GAME_POPUP, () => {
      this.updateBalance(Decimal.add(this.nowBalance, this.gameInfo.TotalPayoff).toFixed(2));
    });
    this.game.events.emit(EVENT_NAME.ENTER_FREE, [true, this.freeTimes]);
    this.isFreeGame = true;
    this.freeDouble = 0;
    this.btnDoubleChance.setVisible(false);
    this.btnBuyFree.setVisible(false);
    this.btnRules.setVisible(false);
    this.btnSpin.setVisible(false);
    this.btnAuto.setVisible(false);
    this.btnCoin.setVisible(false);
    this.slogan.setVisible(false);
    this.settingsPanel.setBetControlInteractive(false);
    this.crashInfos.clearInfoList();
    this.crashInfos.movePosition(config.crashInfo.freeMode.x, config.crashInfo.freeMode.y);
    this.freeTimesFrame.updateFreeTimes(this.freeTimes);
    this.freeGameDouble.setVisible(true);
    this.freeGameScore.setVisible(true);
    this.freeGameScore.updateScore("0.00");
    this.freeGameTotalBalance = this.gameInfo.Balance;
    this.freeGameTotalPayoff = "0.00";
    this.freeGames = this.gameInfo.FreeGame;
    // console.log("enter free game", this.nowBalance, this.freeGameTotalBalance);
    this.footer.updateBet("0.00");
  }

  spinFreeGame() {
    this.freeTimes--;
    this.freeTimesFrame.updateFreeTimes(this.freeTimes);
    this.crashInfos.clearInfoList();
    this.gameInfo = this.freeGames.shift();
    this.freeGameTotalPayoff = Decimal.add(this.freeGameTotalPayoff, this.gameInfo.TotalPayoff).toFixed(2);
    // console.log(this.freeTimes, "free spin", this.gameInfo, this.freeGameTotalPayoff);
    this.cascading.once(EVENT_NAME.CARDS_READY, () => this.checkWinLose());
    this.cascading.fallInCards(this.gameInfo.Cards[0].Columns, true);
    if (this.gameInfo.Double.length > 0) this.time.delayedCall(config.mainCharacter.delay, () => this.artilleryFire());
  }

  endFreeGame() {
    // console.log("end free game", this.nowBalance, this.freeGameTotalBalance, this.freeGameTotalPayoff);
    this.game.events.once(EVENT_NAME.CLOSE_FREE_GAME_POPUP, () => {
      this.btnSpin.setVisible(true);
      this.btnDoubleChance.setVisible(true);
      this.btnBuyFree.setVisible(true);
      this.btnRules.setVisible(true);
      this.slogan.setVisible(true);
      this.settingsPanel.setBetControlInteractive(true);
      this.crashInfos.movePosition(config.crashInfo.x, config.crashInfo.y);
      this.freeTimesFrame.setVisible(false);
      this.freeGameScore.setVisible(false);
      this.freeGameDouble.close();
      this.crashInfos.clearInfoList();
      this.footer.updateBet(this.nowBet);
      this.checkAuto();
    });
    this.game.events.emit(EVENT_NAME.ENTER_FREE, [false, this.freeGameTotalPayoff]);
    this.isFreeGame = false;
    this.gameInfo.Balance = this.freeGameTotalBalance;
  }
}
