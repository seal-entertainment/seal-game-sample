import { CONNECTION_EVENT, EVENT_NAME } from "../constants/events";
import { getAuthPlayerInfo } from "../utils";
import { Connect } from "../min.js/connect.min.js";

const config = {
  width: 720,
  height: 1280,
  blueButton: {
    body: { color: 0x34a3d5, x: 0, y: 0 },
    shadow: { color: 0x2592c2, x: 0, y: 4 },
    width: 360,
    height: 64,
    radius: 10,
  },
  warFire: {
    x: 0,
    y: 758,
    width: 2043,
    height: 522,
    updateX: -2,
  },
  tween: {
    target: [],
    duration: 1000,
    props: {
      alpha: 1,
    },
  },
};
export default class LoadingScene extends Phaser.Scene {
  constructor() {
    super("LoadingScene");
    this.progress = 0;
    this.isSkipLogin = false;
    this.hasRules = false;
    this.hasBalance = false;
    this.LOADING_PROGRESS = {
      CONNECTION_BUILD: 20,
      GET_PLAYERINFO: 30,
      HAS_PLAYERINFO: 50,
      // HAS_RULES: 75,
      HAS_BALANCE: 80,
      READY: 100,
    };
  }

  preload() {
    this.load.atlas("atlas_1", "/images/atlas_1.png", "/images/atlas_1.json");
    this.load.atlas("atlas_2", "/images/atlas_2.png", "/images/atlas_2.json");
    this.load.atlas("atlas_3", "/images/atlas_3.png", "/images/atlas_3.json");
    this.load.atlas("atlas_4", "/images/atlas_4.png", "/images/atlas_4.json");
    this.load.image("bg_base", "/images/background.jpg");
    this.load.image("bg_free", "/images/freegameBG.jpg");
    this.load.image("fire", "/images/fire.png");
    this.load.audio("s_bomb", "/sounds/explosion.mp3");
    this.load.audio("s_bg", "/sounds/warBg.mp3");
  }

  create() {
    this.createGraphicImage();
    this.add.image(this.game.canvas.width / 2, this.game.canvas.height / 2, "bg_base");
    this.bgFree = this.add.image(this.game.canvas.width / 2, this.game.canvas.height / 2, "bg_free");
    this.bgFree.setAlpha(0);
    this.warFire = this.add.tileSprite(
      config.warFire.x,
      config.warFire.y,
      config.warFire.width,
      config.warFire.height,
      "fire"
    );
    this.warFire.setOrigin(0);
    this.warFire.setAlpha(0);
    this.scene.launch("CoveringScene");
    this.connect();
    this.configureListener();
  }

  update() {
    if (this.warFire && this.warFire.alpha > 0) this.warFire.tilePositionX += config.warFire.updateX;
  }

  // 建立 Websocket 連線
  connect() {
    this.connection = new Connect();
    this.connection.buildWithUrl(import.meta.env.VITE_WEBSOCKET_HOST);
    this.updateProgress(this.LOADING_PROGRESS.CONNECTION_BUILD);
    this.connection.on(CONNECTION_EVENT, (context) => this.dispatchEvent(context));
    this.connection.start();
  }

  login() {
    if (this.isSkipLogin) this.enterGame();
    else {
      if (!getAuthPlayerInfo()) {
        this.updateProgress(this.LOADING_PROGRESS.GET_PLAYERINFO);
        const urlParams = new URLSearchParams(window.location.search); // 取得 Game url
        const betType = parseInt(urlParams.get("BetType"));
        this.connection.loginAndGetPlayerInfo(betType ? betType : 8900, urlParams.get("Token")); // call LoginAndGetPlayerInfo API
      } else {
        this.updateProgress(this.LOADING_PROGRESS.HAS_PLAYERINFO);
        this.connection.getBalance(); // call GetBalance API
      }
    }
  }

  configureListener() {
    this.game.events.once(EVENT_NAME.CONNECTION_SUCCESS, () => this.login());
    this.game.events.once(EVENT_NAME.ON_LOGIN, () => this.login());
    this.game.events.once(EVENT_NAME.ON_GET_BALANCE, () => this.enterGame());
    this.game.events.on(EVENT_NAME.GET_RULE, () => this.connection.getGameRule());
    this.game.events.on(EVENT_NAME.PLACE_BET, ([stake, doubleChance]) => this.connection.placeBet(stake, doubleChance));
    this.game.events.on(EVENT_NAME.BUY_FREE_GAME, (stake) => this.connection.buyFreeGame(stake));
    this.game.events.on(EVENT_NAME.ENTER_FREE, ([isShow, updateValue]) => this.showFreeGameBackground(isShow));
  }

  dispatchEvent([evt, data]) {
    // console.log("dispatchEvent", evt, data);
    this.game.events.emit(evt, data);
  }

  updateProgress(value) {
    this.progress = value;
    // console.log("loading", this.progress, "%");
  }

  enterGame() {
    this.updateProgress(this.LOADING_PROGRESS.READY);
    this.scene.launch("MainScene", this.isSkipLogin ? { MinBet: 1, Balance: 10000 } : getAuthPlayerInfo());
  }

  createGraphicImage() {
    const btnMaker = this.make.graphics();
    btnMaker.fillStyle(config.blueButton.shadow.color);
    btnMaker.fillRoundedRect(
      config.blueButton.shadow.x,
      config.blueButton.shadow.y,
      config.blueButton.width - config.blueButton.shadow.x,
      config.blueButton.height - config.blueButton.shadow.y,
      config.blueButton.radius
    );
    btnMaker.fillStyle(config.blueButton.body.color);
    btnMaker.fillRoundedRect(
      config.blueButton.body.x,
      config.blueButton.body.y,
      config.blueButton.width - config.blueButton.body.x,
      config.blueButton.height - config.blueButton.body.y,
      config.blueButton.radius
    );
    btnMaker.generateTexture("bluebutton_up", config.blueButton.width, config.blueButton.height);
    btnMaker.destroy();
  }

  showFreeGameBackground(visible) {
    let tweenConfig = config.tween;
    tweenConfig.targets = [this.warFire, this.bgFree];
    tweenConfig.props.alpha = visible ? 1 : 0;
    tweenConfig.onComplete = (tween) => {
      this.tweens.remove(tween);
    };
    this.tweens.add(tweenConfig);
  }
}
