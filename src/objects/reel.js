import { EVENT_NAME } from "../constants/events";
import { SymbolWithExplosion } from "./symbols/effect-symbol";

const config = {
  symbolsAmount: 5, // 一個reel有幾個symbol
  yDistance: 102, // symbol的上下間距
  fallDelay: [0, 30], // 每個symbol掉落的間隔時間(毫秒)
  readyAtTopDelay: [100, 500], // symbol掉落後，重新回到最頂的等待時間(毫秒)
  fillUpDelay: 1200, // 消除動畫到補排動畫的等待時間(毫秒)
  tween: {
    out: {
      duration: 300,
      props: { y: "+= 510" },
      ease: "Sine.easeIn",
    },
    in: {
      duration: 400,
      props: { y: "+= 510" },
      ease: "Back.easeOut",
      easeParams: [1],
    },
    fillUp: {
      duration: 500,
      ease: "Back.easeOut",
      easeParams: [1], // ease幅度
    },
  },
};
export class Reel extends Phaser.GameObjects.Container {
  constructor(scene, index) {
    super(scene);
    this.scene.add.existing(this);
    this.symbols = [];
    this.canNext = true;
    this.reelIndex = index;
    this.isNewCards = false;
    this.setY(config.symbolsAmount * config.yDistance);
    // 由下而上
    for (let i = 0; i < config.symbolsAmount; i++) {
      const symbol = new SymbolWithExplosion(scene, Phaser.Math.Between(0, 8));
      symbol.setY(i * -config.yDistance);
      this.add(symbol);
      this.symbols.push(symbol);
    }
  }

  //---------清除舊局，補上新局---------
  start(cards, isTurbo) {
    this.cards = cards;
    this.fallDown(isTurbo, true);
  }

  fallDown(isTurbo, isFirst) {
    let tweenConfig = isFirst ? config.tween.out : config.tween.in;
    tweenConfig.targets = this.symbols;
    const eachDelay = isTurbo ? config.fallDelay[0] : config.fallDelay[1];
    tweenConfig.delay = this.scene.tweens.stagger(eachDelay);
    tweenConfig.onComplete = (tween) => {
      this.checkInOrOut(isTurbo);
      this.scene.tweens.remove(tween);
    };
    this.scene.tweens.add(tweenConfig);
  }

  checkInOrOut(isTurbo) {
    if (!this.isNewCards) {
      this.readyAtTop();
      const delay = isTurbo ? config.readyAtTopDelay[0] : config.readyAtTopDelay[1];
      this.scene.time.delayedCall(delay, () => this.fallDown(isTurbo, false));
    } else {
      this.emit(EVENT_NAME.CARDS_READY);
    }
    this.isNewCards = !this.isNewCards;
  }

  readyAtTop() {
    for (let i = 0; i < this.symbols.length; i++) {
      this.symbols[i].setY(config.symbolsAmount * -config.yDistance + i * -config.yDistance);
      this.symbols[i].changeCard(this.cards[i]);
    }
  }

  //---------消除&補牌動畫-------------
  addSymbolToTop(array) {
    this.canNext = array.length === 0;
    for (let i = 0; i < array.length; i++) {
      const symbol = new SymbolWithExplosion(this.scene, array[array.length - 1 - i]);
      this.symbols.push(symbol);
      symbol.setY((config.symbolsAmount + i) * -config.yDistance);
      this.add(symbol);
    }
  }

  crashSymbols(ID) {
    const crashIndexs = [];
    for (let i = 0; i < config.symbolsAmount; i++) {
      for (let j = 0; j < ID.length; j++) {
        if (this.symbols[i].card === ID[j]) {
          this.symbols[i].crash();
          crashIndexs.unshift(i); //因為從前頭splice會導致後面的index改變，故用unshift而非push
        }
      }
    }
    this.scene.time.delayedCall(config.fillUpDelay, () => {
      this.removeCrashSymbols(crashIndexs);
      this.fillUpCards();
    });
  }

  removeCrashSymbols(crashIndexs) {
    for (let i = 0; i < crashIndexs.length; i++) {
      this.symbols[crashIndexs[i]].destroy();
      this.symbols.splice(crashIndexs[i], 1);
    }
  }

  fillUpCards() {
    let delayCount = 0;
    let completeCount = 0;
    const checkComplete = () => {
      completeCount++;
      if (completeCount === delayCount) {
        this.canNext = true;
        this.emit(EVENT_NAME.CAN_NEXT, this.reelIndex);
      } // 當所有symbols動畫都演完後就可以告訴父層：我準備好 can next!
    };
    for (let i = 0; i < this.symbols.length; i++) {
      if (Math.round(this.symbols[i].y) !== i * -config.yDistance) {
        let tweenConfig = config.tween.fillUp;
        tweenConfig.targets = [this.symbols[i]];
        tweenConfig.y = i * -config.yDistance;
        tweenConfig.delay = delayCount * config.fallDelay;
        tweenConfig.onComplete = (tween) => {
          this.scene.tweens.remove(tween);
          checkComplete();
        };
        this.scene.tweens.add(tweenConfig);
        delayCount++;
      }
    }
  }

  playScatterAnimation() {
    for (let i = 0; i < config.symbolsAmount; i++) {
      if (this.symbols[i].isScatter) {
        this.symbols[i].playScatter();
      }
    }
  }

  playDoubleAnimation() {
    for (let i = 0; i < config.symbolsAmount; i++) {
      if (this.symbols[i].isDouble) {
        this.symbols[i].playDouble();
      }
    }
  }

  getSymbol(index) {
    return this.symbols[index];
  }
}
