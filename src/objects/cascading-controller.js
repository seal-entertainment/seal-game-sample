import { EVENT_NAME } from "../constants/events";
import { Reel } from "./reel";
import { Digits } from "./digits";
import Decimal from "decimal.js";

const config = {
  reelsAmount: 6,
  xDistance: 105,
  reelsDelay: [0, 100],
  nextTurnDelay: 500,
  explosionDelay: [600, 1500],
  totalDoubleDelay: 500,
  mask: {
    x: -52,
    y: 50,
    width: 632,
    height: 514,
    style: { fillStyle: { color: 0xff0000, alpha: 0.3 } },
  },
  score: {
    x: 260,
    y: 250,
    sameTimeDelay: 100,
  },
  tween: {
    score: {
      props: {
        y: {
          value: 210,
          duration: 800,
        },
        alpha: {
          value: 1,
          duration: 100,
          yoyo: true,
          hold: 600,
        },
      },
    },
  },
};

export class CascadingController extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.cards = [];
    this.reels = [];
    this.nowTurn = 0;
    this.turnPayoff = [];

    for (let i = 0; i < config.reelsAmount; i++) {
      const reel = new Reel(scene, i);
      reel.setX(i * config.xDistance);
      this.add(reel);
      this.reels.push(reel);
      reel.on(EVENT_NAME.CAN_NEXT, () => this.checkNext());
    }
    this.addMask();
  }

  addMask() {
    const shape = this.scene.make.graphics(config.mask.style);
    shape.fillRect(config.mask.x, config.mask.y, config.mask.width, config.mask.height);
    shape.setPosition(this.x, this.y);
    const mask = shape.createGeometryMask();
    this.setMask(mask);
  }

  //清除舊局，補上新局
  fallInCards(cards, isTurbo) {
    let completedCount = 0;
    const waitAllReelsCompleted = () => {
      completedCount++;
      if (completedCount === this.reels.length) this.emit(EVENT_NAME.CARDS_READY);
    };
    for (let i = 0; i < this.reels.length; i++) {
      this.reels[i].once(EVENT_NAME.CARDS_READY, () => waitAllReelsCompleted());
      this.scene.time.delayedCall(i * (isTurbo ? config.reelsDelay[0] : config.reelsDelay[1]), () =>
        this.reels[i].start(cards[i].Symbol, isTurbo)
      );
    }
  }

  //---------消除&補牌&得分動畫-------------
  playWinAnimation(cards) {
    this.cards = cards;
    // console.log("Show crush animation", cards);
    this.nowTurn = 0;
    this.totalWin = 0;
    this.nextTurn();
  }

  nextTurn() {
    // console.log("nextTurn", this.nowTurn, this.cards.length);
    this.nowTurn++;
    for (let i = 0; i < this.reels.length; i++) {
      const crashAmount = this.cards[this.nowTurn - 1].Columns[i].RemoveAmount;
      const array = this.cards[this.nowTurn].Columns[i].Symbol.slice(0, crashAmount);
      this.reels[i].addSymbolToTop(array);
    }
    this.crashSymbols(this.nowTurn - 1);
  }

  crashSymbols(turn) {
    const hitAmount = this.cards[turn].HitAmount;
    const hitID = this.cards[turn].HitSymbol;
    const turnPayoff = this.cards[turn].Payoff;
    // console.log("crashSymbols", hitID, turnPayoff);
    for (let i = 0; i < hitID.length; i++) {
      this.scene.time.delayedCall(i * config.score.sameTimeDelay, () => {
        this.scene.events.emit(EVENT_NAME.CRASH, [hitAmount[i], hitID[i], turnPayoff[i]]);
        this.scene.events.emit(EVENT_NAME.UPDATE_VALUE, turnPayoff[i]);
      });
    }
    for (let i = 0; i < this.reels.length; i++) {
      this.reels[i].crashSymbols(hitID);
    }
    this.scene.time.delayedCall(config.explosionDelay[0], () => this.scene.sound.get("s_bomb").play());
    this.showPayoff(JSON.parse(JSON.stringify(turnPayoff)));
  }

  checkNext() {
    for (let i = this.reels.length - 1; i >= 0; i--) {
      if (!this.reels[i].canNext) return; //如果有reel還沒有準備好nextTurn，就等他canNext
    }
    this.scene.time.delayedCall(config.nextTurnDelay, () => {
      if (this.nowTurn >= this.cards.length - 1) {
        // console.log("turn end", this.nowTurn, this.cards.length);
        this.emit(EVENT_NAME.ANIMATION_COMPLETED, this.totalWin);
      } else {
        this.nextTurn();
      }
    });
  }

  showPayoff(payoffs) {
    // console.log("ShowPayoff", payoffs);
    let payoff = new Decimal(payoffs.pop()).toFixed(2);
    while (payoffs.length > 0) {
      payoff = Decimal.add(payoff, payoffs.pop()).toFixed(2);
      this.totalWin = Decimal.add(this.totalWin, payoff).toFixed(2);
    }
    const score = new Digits(this.scene, config.score.x, config.score.y, "atlas_1", "orange_number/");
    this.add(score);
    score.displayWithString(payoff);
    let tweenConfig = config.tween.score;
    tweenConfig.targets = score;
    tweenConfig.onComplete = (tween) => {
      this.scene.tweens.remove(tween);
      score.destroy();
    };
    this.scene.tweens.add(config.tween.score);
  }
  //--------------------------------------
  winScatter() {
    for (let i = 0; i < config.reelsAmount; i++) {
      this.reels[i].playScatterAnimation();
    }
    this.scene.time.delayedCall(config.explosionDelay[1], () => this.scene.sound.get("s_bomb").play());
  }

  winDouble(doubles) {
    // console.log("Show double animation");
    let double = 0;
    for (let i = 0; i < doubles.length; i++) {
      double += doubles[i];
    }
    for (let i = 0; i < config.reelsAmount; i++) {
      this.reels[i].playDoubleAnimation();
    }
    this.scene.time.delayedCall(config.totalDoubleDelay, () => this.emit(EVENT_NAME.DOUBLE_COMPLETED, double));
  }

  getSymbol(column, row) {
    return this.reels[column].getSymbol(row);
  }
}
