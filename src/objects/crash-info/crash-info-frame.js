import Decimal from "decimal.js";
import { EVENT_NAME } from "../../constants/events";
import { CrashInfo } from "./crash-info";

const config = {
  background: {
    style: { fillStyle: { color: 0x212b27 }, lineStyle: { color: 0xffffff } },
    width: 220,
    height: 250,
    radius: 12,
    lineWidth: 1,
  },
  maxAmount: 5,
  tween: {
    listDown: {
      duration: 200,
      props: { y: "+= 50" },
    },
    clear: {
      duration: 200,
      props: { y: "+= 300" },
      eachDelay: 30,
    },
    fallDown: {
      duration: 500,
      ease: "Bounce.easeOut",
    },
  },
  infoHeight: 50,
};

export class CrashInfoFrame extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const graphic = this.scene.add.graphics(config.background.style);
    graphic.fillRoundedRect(0, 0, config.background.width, config.background.height, config.background.radius);
    graphic.strokeRoundedRect(0, 0, config.background.width, config.background.height, config.background.radius);
    this.infoList = this.scene.add.container(0, config.background.height);
    this.add([graphic, this.infoList]);
    this.addMask(x, y);
    // -----dev test-----
    // for (let i = 0; i < 7; i++) {
    //   this.scene.time.delayedCall(i * 1000, () => this.moveDownList(8 + i, i, 0.5 + i * 0.25));
    // }
    this.scene.events.on(EVENT_NAME.CRASH, ([crashAmount, card, payoff]) =>
      this.moveDownList(crashAmount, card, payoff)
    );
  }

  addMask(x, y) {
    const shape = this.scene.make.graphics();
    shape.fillStyle(0xff0000, 0.3);
    shape.fillRoundedRect(
      config.background.lineWidth,
      config.background.lineWidth,
      config.background.width - 2 * config.background.lineWidth,
      config.background.height - 2 * config.background.lineWidth,
      config.background.radius
    );
    shape.setPosition(x, y);
    const mask = shape.createGeometryMask();
    this.infoList.setMask(mask);
  }

  movePosition(x, y) {
    this.setPosition(x, y);
    this.addMask(x, y);
  }

  moveDownList(crashAmount, card, payoff) {
    if (this.infoList.list.length >= config.maxAmount) {
      let tweenConfig = config.tween.listDown;
      tweenConfig.targets = [this.infoList];
      tweenConfig.onComplete = (tween) => {
        this.addInfo(crashAmount, card, payoff);
        this.scene.tweens.remove(tween);
      };
      this.scene.tweens.add(tweenConfig);
    } else this.addInfo(crashAmount, card, payoff);
  }

  addInfo(crashAmount, card, payoff) {
    const y = (this.infoList.length + config.maxAmount + 1) * -config.infoHeight;
    const info = new CrashInfo(this.scene, 0, y, crashAmount, card, payoff);
    this.infoList.add(info);
    this.fallInfo(info);
  }

  fallInfo(target) {
    let tweenConfig = config.tween.fallDown;
    tweenConfig.targets = [target];
    tweenConfig.y = {
      getEnd: () => {
        return Decimal.mul(this.infoList.length, -config.infoHeight).toNumber();
      },
    };
    tweenConfig.onComplete = (tween) => this.scene.tweens.remove(tween);
    this.scene.tweens.add(tweenConfig);
  }

  clearInfoList() {
    let tweenConfig = config.tween.clear;
    tweenConfig.targets = this.infoList.list;
    tweenConfig.delay = this.scene.tweens.stagger(config.tween.clear.eachDelay);
    tweenConfig.onComplete = (tween) => {
      this.scene.tweens.remove(tween);
      this.infoList.removeAll(true);
      this.infoList.y = config.background.height;
    };
    this.scene.tweens.add(tweenConfig);
  }
}
