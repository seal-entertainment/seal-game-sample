import Decimal from "decimal.js";
import { Digits } from "../digits";
import { BasicSymbol } from "../symbols/basic-symbol";

const config = {
  textureKey: "atlas_1",
  crash: { x: 30, y: 8 },
  payoff: { x: 205, y: 8 },
  symbol: { x: 82, y: 25, scale: 0.5 },
};

export class CrashInfo extends Phaser.GameObjects.Container {
  constructor(scene, x, y, crashAmount, card, payoff) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const bg = this.scene.add.image(0, 0, config.textureKey, "crashrect").setOrigin(0);
    this.width = bg.width;
    this.height = bg.height;
    this.crashAmount = new Digits(scene, config.crash.x, config.crash.y, config.textureKey, "orange_number/");
    this.crashAmount.displayWithString(crashAmount.toString());
    this.symbol = new BasicSymbol(scene, card, config.SYMBOLS_TEXTURE);
    this.symbol.setPosition(config.symbol.x, config.symbol.y).setScale(config.symbol.scale);
    this.payoff = new Digits(scene, config.payoff.x, config.payoff.y, config.textureKey, "orange_number/");
    this.payoff.displayWithString(new Decimal(payoff).toFixed(2), "right");
    this.add([bg, this.crashAmount, this.symbol, this.payoff]);
  }
}
