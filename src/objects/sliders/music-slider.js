import Decimal from "decimal.js";
import { EVENT_NAME } from "../../constants/events";
import { changeImageByState, switchButton } from "../../utils";
import { BasicImageButton } from "../buttons/basic-image-button";
import { SliderWithMask } from "./slider-with-mask";

const config = {
  textureKey: "atlas_1",
  slider: {
    x: 40,
    y: 0,
    track: {
      x: 0,
      y: 0,
      frameKey: "maxitrack",
    },
    thumb: {
      x: 2,
      y: -2,
      frameKey: "minitrack",
    },
    touchAreaRadius: 20,
  },
  btn: {
    minus: {
      x: 10,
      y: 20,
      frameKey: "minus_up",
    },
    plus: {
      x: 320,
      y: 20,
      frameKey: "plus_up",
    },
  },
};

export class MusicSlider extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.value = 1;

    this.slider = new SliderWithMask(scene, config.slider.x, config.slider.y);
    this.slider.setTrackImage(
      config.slider.track.x,
      config.slider.track.y,
      config.textureKey,
      config.slider.track.frameKey
    );
    this.slider.setThumbImage(
      config.slider.thumb.x,
      config.slider.thumb.y,
      config.textureKey,
      config.slider.thumb.frameKey
    );
    this.slider.setTouchArea(config.slider.touchAreaRadius);
    this.btnMinus = new BasicImageButton(
      scene,
      config.btn.minus.x,
      config.btn.minus.y,
      config.textureKey,
      config.btn.minus.frameKey
    );
    this.btnPlus = new BasicImageButton(
      scene,
      config.btn.plus.x,
      config.btn.plus.y,
      config.textureKey,
      config.btn.plus.frameKey
    );
    this.add([this.slider, this.btnMinus, this.btnPlus]);

    this.slider.on(EVENT_NAME.UPDATE_VALUE, (value) => this.updateValue(value));
    this.btnMinus.onButtonUp = () => {
      const value = Decimal.sub(this.value, 0.1).toNumber();
      this.updateValue(value);
    };
    this.btnPlus.onButtonUp = () => {
      const value = Decimal.add(this.value, 0.1).toNumber();
      this.updateValue(value);
    };
    switchButton(this.btnPlus, false);
    changeImageByState(this.btnPlus, "no");
  }

  updateValue(value) {
    this.slider.updateThumbByValue(value);
    this.value = value;
    switchButton(this.btnPlus, value < 1);
    changeImageByState(this.btnPlus, value < 1 ? "up" : "no");
    switchButton(this.btnMinus, value > 0);
    changeImageByState(this.btnMinus, value > 0 ? "up" : "no");
    this.emit(EVENT_NAME.UPDATE_VALUE, value);
  }

  setWorldMask(worldX, worldY) {
    this.slider.setThumbMask(
      worldX + config.slider.thumb.x,
      worldY + config.slider.thumb.y,
      config.textureKey,
      config.slider.thumb.frameKey
    );
  }
}
