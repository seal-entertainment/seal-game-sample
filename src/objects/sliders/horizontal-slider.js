import Decimal from "decimal.js";
import { EVENT_NAME } from "../../constants/events";
import { BasicImageButton } from "../buttons/basic-image-button";

export class HorizontalSlider extends Phaser.GameObjects.Container {
  constructor(scene, x, y, trackImg, thumbImg) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.setValue();
    if (trackImg) this.setTrackImage(0, 0, trackImg.texture, trackImg.frame);
    if (thumbImg) this.setThumbImage(0, 0, thumbImg.texture, thumbImg.frame);
  }

  setValue(value) {
    this.emit(EVENT_NAME.UPDATE_VALUE, value);
  }

  setTrackImage(x, y, texture, frame) {
    this.track = this.scene.add.image(x, y, texture, frame);
    this.add(this.track);
  }

  setThumbImage(x, y, texture, frame) {
    this.thumb = new BasicImageButton(this.scene, x, y, texture, frame);
    this.add(this.thumb);
    if (this.track) this.addDragEventListener();
  }

  addDragEventListener() {
    this.scene.input.setDraggable(this.thumb);
    // 內建 function，有固定參數順序
    this.thumb.onButtonDrag = (point, dragX) => {
      const xStart = this.track.x - this.track.width / 2;
      const xEnd = this.track.x + this.track.width / 2;
      dragX = Phaser.Math.Clamp(dragX, xStart, xEnd);
      this.thumb.x = dragX;
      const value = Decimal.div(Decimal.sub(dragX, xStart), Decimal.sub(xEnd, xStart)).toFixed(2);
      this.setValue(value);
    };
  }
}
