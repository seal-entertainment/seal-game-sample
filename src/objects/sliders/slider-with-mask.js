import Decimal from "decimal.js";
import { EVENT_NAME } from "../../constants/events";

export class SliderWithMask extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
  }

  updateThumbByValue(value) {
    const range = Decimal.sub(this.track.width, this.touchAreaRadius);
    const X = Decimal.sub(value, 1).mul(range).toNumber();
    this.thumb.setX(X);
    this.touchArea.setX(Decimal.mul(value, range).toNumber());
  }

  setTrackImage(x, y, texture, frame) {
    this.track = this.scene.add.image(x, y, texture, frame).setOrigin(0);
    this.add(this.track);
  }

  setThumbImage(x, y, texture, frame) {
    this.thumb = this.scene.add.image(x, y, texture, frame).setOrigin(0);
    this.add(this.thumb);
  }

  setTouchArea(radius) {
    this.touchAreaRadius = radius;
    this.touchArea = this.scene.add.zone(this.track.width - radius, radius, radius * 2, radius * 2);
    this.touchArea.setInteractive({ useHandCursor: true });
    this.add(this.touchArea);
    if (this.track) this.addDragEventListener();
  }

  setThumbMask(x, y, texture, frame) {
    const img = this.scene.add.image(x, y, texture, frame).setVisible(false).setOrigin(0);
    this.thumb.setMask(img.createBitmapMask());
  }

  addDragEventListener() {
    this.scene.input.setDraggable(this.touchArea);
    // 內建 function，有固定參數順序
    this.touchArea.on(Phaser.Input.Events.DRAG, (point, dragX) => {
      const xStart = 1 - this.touchAreaRadius;
      const xEnd = this.track.width - this.touchAreaRadius;
      dragX = Phaser.Math.Clamp(dragX, xStart, xEnd);
      const value = new Decimal(dragX).plus(this.touchAreaRadius).div(this.track.width).toFixed(1);
      this.emit(EVENT_NAME.UPDATE_VALUE, value);
    });
  }
}
