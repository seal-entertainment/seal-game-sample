export class BasicTextButton extends Phaser.GameObjects.Text {
  constructor(scene, x, y, text, style) {
    super(scene, x, y, text, style);
    this.scene.add.existing(this);
    this.setInteractive({ useHandCursor: true })
      .on(Phaser.Input.Events.POINTER_OVER, () => this.onButtonOver())
      .on(Phaser.Input.Events.POINTER_OUT, () => this.onButtonOut())
      .on(Phaser.Input.Events.POINTER_DOWN, () => this.onButtonDown())
      .on(Phaser.Input.Events.POINTER_UP, () => this.onButtonUp());
  }

  onButtonOver() {}

  onButtonOut() {}

  onButtonDown() {}

  onButtonUp() {}
}
