import { BasicImageButton } from "./basic-image-button";

const config = {
  text: {
    x: 0,
    y: 20,
    style: {
      fontSize: "24px",
      fontFamily: "Helvetica",
      align: "center",
      fixedWidth: 360,
    },
  },
};
export class BlueRoundedRectButton extends Phaser.GameObjects.Container {
  constructor(scene, x, y, buttonText) {
    super(scene, x, y);
    this.scene.add.existing(this);

    this.btn = new BasicImageButton(scene, 0, 0, "bluebutton_up").setOrigin(0);
    this.text = this.scene.add.text(config.text.x, config.text.y, buttonText, config.text.style);
    this.add([this.btn, this.text]);
  }

  onButtonUp(fun) {
    this.btn.onButtonUp = () => fun();
  }

  setText(str) {
    this.text.setText(str);
  }
}
