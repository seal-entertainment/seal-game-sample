import { addLinearGradient, changeImageByState, switchButton } from "../../utils";
import { BasicImageButton } from "./basic-image-button";
const config = {
  bet: {
    x: 24,
    y: 17,
    str: "BET",
  },
  tittle: {
    x: 115,
    y: 27,
    str: "DOUBLE THE CHANCE\nTO WIN FEATURE",
  },
  price: {
    x: 28,
    y: 37,
  },
  style: {
    gray: {
      font: "700 16px Helvetica",
      align: "left",
      lineSpacing: 2,
      linearGradient: [
        { stop: 0, color: "#FFFFFF" },
        { stop: 1, color: "#A2B9CC" },
      ],
    },
    orange: {
      font: "700 26px Helvetica",
      align: "center",
      fixedWidth: 80,
      wordWrap: { width: 80 },
      lineSpacing: 4,
      linearGradient: [
        { stop: 0, color: "#FFDE00" },
        { stop: 1, color: "#FF8400" },
      ],
    },
  },
};
export class DoubleChanceButton extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.btn = new BasicImageButton(scene, 0, 0, "atlas_1", "doubleChance_off").setOrigin(0);
    const tittle = this.scene.add.text(config.tittle.x, config.tittle.y, config.tittle.str, config.style.gray);
    addLinearGradient(tittle, config.style.gray.linearGradient);
    const betText = this.scene.add.text(config.bet.x, config.bet.y, config.bet.str, config.style.gray);
    addLinearGradient(betText, config.style.gray.linearGradient);
    this.price = this.scene.add.text(config.price.x, config.price.y, "1.25", config.style.orange);
    addLinearGradient(this.price, config.style.orange.linearGradient);
    this.add([this.btn, tittle, betText, this.price]);
  }

  updatePrice(str) {
    this.price.setText(str);
  }

  onButtonUp(fun) {
    this.btn.onButtonUp = () => {
      this.btn.isClick = !this.btn.isClick;
      changeImageByState(this.btn, this.btn.isClick ? "on" : "off");
      fun();
    };
  }

  lock(on) {
    this.setAlpha(on ? 0.7 : 1);
    if (on) this.btn.disableInteractive();
    else this.btn.setInteractive();
  }
}
