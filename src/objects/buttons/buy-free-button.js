import { Digits } from "../digits";
import { BasicImageButton } from "./basic-image-button";

const config = {
  textureKey: "atlas_1",
  tittle: { x: 23, y: 13 },
  digits: { x: 100, y: 36, spaceSpacing: 10 },
};

export class BuyFreeButton extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.btn = new BasicImageButton(scene, 0, 0, config.textureKey, "metal").setOrigin(0);
    const tittle = this.scene.add.image(config.tittle.x, config.tittle.y, config.textureKey, "buy").setOrigin(0);
    this.price = new Digits(scene, config.digits.x, config.digits.y, config.textureKey, "orange_number/");
    this.add([this.btn, tittle, this.price]);
    this.price.spaceSpacing = config.digits.spaceSpacing;
    this.price.setCharacter(12, "y");
    this.price.displayWithString("y 800");
  }

  updatePrice(str) {
    this.price.displayWithString(str);
  }

  onButtonUp(fun) {
    this.btn.onButtonUp = () => fun();
  }

  lock(on) {
    this.setAlpha(on ? 0.7 : 1);
    if (on) this.btn.disableInteractive();
    else this.btn.setInteractive();
  }
}
