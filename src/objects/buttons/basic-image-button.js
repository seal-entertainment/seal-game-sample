// eslint-disable-next-line max-classes-per-file
import Phaser from "phaser";

export class BasicImageButton extends Phaser.GameObjects.Image {
  constructor(scene, x, y, texture, frame) {
    super(scene, x, y, texture, frame);
    this.scene.add.existing(this);
    this.isClick = false;
    this.onAllEventListener();
  }

  onAllEventListener() {
    this.setInteractive({ useHandCursor: true }) // 鼠標在點擊區域會變成手指
      .on(Phaser.Input.Events.POINTER_OVER, () => this.onButtonOver())
      .on(Phaser.Input.Events.POINTER_OUT, () => this.onButtonOut())
      .on(Phaser.Input.Events.POINTER_DOWN, () => this.onButtonDown())
      .on(Phaser.Input.Events.POINTER_UP, () => this.onButtonUp())
      .on(Phaser.Input.Events.DRAG, (point, dragX, dragY) => this.onButtonDrag(point, dragX, dragY));
  }
  // pointer over event 實作
  onButtonOver() {}
  // pointer over event 實作
  onButtonOut() {}
  // pointer over event 實作
  onButtonDown() {}
  // pointer over event 實作
  onButtonUp() {}
  // drag event 實作
  onButtonDrag() {}
}
