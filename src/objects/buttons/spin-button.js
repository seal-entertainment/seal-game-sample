import { EVENT_NAME } from "../../constants/events";
import { changeImageByState } from "../../utils";
import { BasicImageButton } from "./basic-image-button";
import { Digits } from "../digits";

const config = {
  textureKey: "atlas_1",
  digits: { x: -1, y: -16 },
};

export class SpinButton extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.isAuto = false;
    this.isLock = false;
    this.btn = new BasicImageButton(scene, 0, 0, config.textureKey, "spin_up");
    this.autoTimes = new Digits(
      scene,
      config.digits.x,
      config.digits.y,
      config.textureKey,
      "orange_number/"
    ).setVisible(true);
    this.add([this.btn, this.autoTimes]);
    // this.autoTimes.displayWithString("55");
  }

  updateAutoTimes(value) {
    this.isAuto = true;
    changeImageByState(this.btn, "auto");
    this.autoTimes.setVisible(true);
    this.autoTimes.displayWithString(value.toString());
  }

  closeAuto() {
    this.setVisible(true);
    this.isAuto = false;
    changeImageByState(this.btn, "up");
    this.autoTimes.setVisible(false);
  }

  onButtonUp(fun) {
    this.btn.onButtonUp = () => {
      if (this.isAuto) {
        this.emit(EVENT_NAME.STOP_AUTO);
        this.setVisible(false);
      } else fun();
    };
  }

  lock(on) {
    if (on && !this.isAuto) this.btn.disableInteractive();
    else this.btn.setInteractive();
  }
}
