import Decimal from "decimal.js";
import { EVENT_NAME } from "../constants/events";
import { Digits } from "./digits";

const config = {
  text: {
    x: 0,
    y: -10,
    style: {
      fontSize: "20px",
      fontFamily: "Helvetica",
      color: "#EFEFEF",
      stroke: "#46484D",
      strokeThickness: 4,
      align: "center",
      fixedWidth: 376,
    },
  },
  digit: {
    x: 188,
    y: 15,
    scale: 1.2,
  },
};

export class TumbleScore extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const bg = this.scene.add.image(0, 0, "atlas_1", "tumblebox").setOrigin(0);
    const text = this.scene.add.text(config.text.x, config.text.y, "TUMBLE WIN", config.text.style);
    this.score = new Digits(scene, config.digit.x, config.digit.y, "atlas_1", "orange_number/");
    this.score.setScale(config.digit.scale);
    this.score.displayWithString("0.80");
    this.add([bg, text, this.score]);
    this.close();

    this.scene.events.on(EVENT_NAME.UPDATE_VALUE, (payoff) => {
      this.nowScore = Decimal.add(this.nowScore, payoff).toFixed(2);
      this.updateScore(this.nowScore);
    });
  }

  updateScore(str) {
    this.setVisible(true);
    this.score.displayWithString(str);
  }

  close() {
    this.nowScore = "0.00";
    this.score.displayWithString(this.nowScore);
    this.setVisible(false);
  }
}
