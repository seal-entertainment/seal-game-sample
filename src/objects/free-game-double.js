import { Digits } from "./digits";

const config = {
  digit: {
    x: 100,
    y: 40,
    scale: [1.8, 1.5],
  },
  text: {
    x: 0,
    y: 100,
    style: {
      fontSize: "24px",
      fontFamily: "Helvetica",
      color: "#FFF5CC",
      stroke: "#2F412C",
      strokeThickness: 5,
      align: "center",
      fixedWidth: 200,
      wordWrap: { width: 150 },
      lineSpacing: -6,
    },
  },
};

export class FreeGameDouble extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const background = this.scene.add.image(0, 0, "atlas_4", "greenbadge").setOrigin(0);
    const text = this.scene.add.text(0, config.text.y, "TOTAL MULTIPLIER", config.text.style);
    this.double = new Digits(scene, config.digit.x, config.digit.y, "atlas_1", "double/");
    this.double.setCharacter(12, "x");
    this.double.setStringSpacing(0);
    this.double.setDigitSpacing(0);
    this.double.setScale(config.digit.scale[0]);
    this.add([background, text, this.double]);
    this.double.displayWithString("888x");
    this.close();
  }

  updateDouble(value) {
    if (value >= 99) this.double.setScale(config.digit.scale[1]);
    else this.double.setScale(config.digit.scale[0]);
    this.double.displayWithString(value + "x");
  }

  close() {
    this.double.displayWithString("");
    this.double.setScale(config.digit.scale[0]);
    this.setVisible(false);
  }
}
