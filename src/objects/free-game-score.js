const config = {
  text: {
    x: 0,
    y: 3,
    style: {
      color: "#35190D",
      stroke: "#35190D",
      fontSize: "30px",
      fontFamily: "Helvetica",
      strokeThickness: 2,
      align: "center",
      fixedWidth: 560,
    },
  },
};

export class FreeGameScore extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const bg = this.scene.add.image(0, 0, "atlas_4", "white").setOrigin(0);
    this.text = this.scene.add.text(config.text.x, config.text.y, "WIN 0.00", config.text.style);
    this.add([bg, this.text]);
    this.setVisible(false);
  }

  updateScore(str) {
    this.text.setText("WIN " + str);
  }
}
