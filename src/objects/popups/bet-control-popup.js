import { BasicPopup } from "./basic-popup";
import { BetControl } from "./bet-control";

const config = {
  width: 600,
  height: 350,
  betControl: {
    x: 120,
    y: 156,
  },
};

export class BetControlPopup extends BasicPopup {
  constructor(scene, x, y, outsideBtn) {
    super(scene, x, y, config.width, config.height, outsideBtn);
    this.tittle.setText("BET MULTIPLIER 20x");
    this.betControl = new BetControl(scene, config.betControl.x, config.betControl.y);
    this.add(this.betControl);
  }
}
