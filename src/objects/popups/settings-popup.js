import { EVENT_NAME } from "../../constants/events";
import { BetControl } from "./bet-control";
import { BasicPopup } from "./basic-popup";
import { MusicSlider } from "../sliders/music-slider";

const config = {
  width: 600,
  height: 660,
  text: {
    style: {
      fontSize: "30px",
      fontFamily: "Helvetica",
      strokeThickness: 1,
    },
    music: {
      x: 40,
      y: 160,
    },
    sound: {
      x: 40,
      y: 249,
    },
    betSettings: {
      x: 212,
      y: 386,
    },
  },
  slider: {
    music: {
      x: 230,
      y: 166,
      mask: {
        x: 330,
        y: 416,
      },
    },
    sound: {
      x: 230,
      y: 246,
      mask: {
        x: 330,
        y: 496,
      },
    },
  },
  line: {
    style: { lineStyle: { alpha: 0.5 } },
    x1: 40,
    y1: 346,
    x2: 560,
    y2: 346,
  },
  betControl: { x: 120, y: 460 },
};

export class SettingsPopup extends BasicPopup {
  constructor(scene, x, y, outsideBtn) {
    super(scene, x, y, config.width, config.height, outsideBtn);
    this.tittle.setText("SYSTEM SETTINGS");

    const musicText = this.scene.add.text(config.text.music.x, config.text.music.y, "Music", config.text.style);
    const soundText = this.scene.add.text(config.text.sound.x, config.text.sound.y, "Sound", config.text.style);
    this.musicSlider = new MusicSlider(scene, config.slider.music.x, config.slider.music.y);
    this.soundSlider = new MusicSlider(scene, config.slider.sound.x, config.slider.sound.y);
    this.musicSlider.setWorldMask(config.slider.music.mask.x, config.slider.music.mask.y);
    this.soundSlider.setWorldMask(config.slider.sound.mask.x, config.slider.sound.mask.y);
    this.add([musicText, soundText, this.musicSlider, this.soundSlider]);
    this.music = this.scene.sound.add("s_bg", { loop: true, volume: 1 });
    this.sound = this.scene.sound.add("s_bomb", { volume: 1 });
    this.musicSlider.on(EVENT_NAME.UPDATE_VALUE, (value) => this.music.setVolume(value));
    this.soundSlider.on(EVENT_NAME.UPDATE_VALUE, (value) => this.sound.setVolume(value));
    this.music.play();

    const graphic = this.scene.add.graphics(config.line.style);
    graphic.lineBetween(config.line.x1, config.line.y1, config.line.x2, config.line.y2);
    const subtitle = this.scene.add.text(
      config.text.betSettings.x,
      config.text.betSettings.y,
      "Bet Settings",
      config.text.style
    );
    this.betControl = new BetControl(scene, config.betControl.x, config.betControl.y);
    this.add([graphic, subtitle, this.betControl]);
  }

  setBetControlInteractive(on) {
    this.betControl.switchButton(on);
  }
}
