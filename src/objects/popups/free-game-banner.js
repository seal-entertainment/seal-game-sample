import { EVENT_NAME } from "../../constants/events";
import { BasicImageButton } from "../buttons/basic-image-button";
import { Digits } from "../digits";

const config = {
  light: { x: 0, y: 0 },
  banner: { x: 0, y: 0 },
  tittle: { x: 67, y: 30 },
  frame: {
    x: 186,
    y: 158,
    width: 240,
    height: 80,
    style: { fillStyle: { color: 0x103e17, alpha: 0.3 }, lineStyle: { color: 0xfff8c8, width: 1 } },
  },
  textStyle: {
    fontSize: "30px",
    fontFamily: "Helvetica",
    strokeThickness: 1,
    align: "center",
    fixedWidth: 612,
  },
  top: {
    x: 0,
    y: 114,
  },
  bottom: {
    x: 0,
    y: 247,
  },
  digit: {
    x: 305,
    y: 165,
    scale: 2,
  },
  closeButton: {
    x: 588,
    y: 72,
  },
  closeDelay: 5000,
};

export class FreeGamePopup extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);

    const bg = this.scene.add.image(config.banner.x, config.banner.y, "atlas_4", "banner").setOrigin(0);
    this.light = this.scene.add.image(config.light.x, config.light.y, "atlas_4", "banner_light").setOrigin(0);
    const tittle = this.scene.add.image(config.tittle.x, config.tittle.y, "atlas_4", "congrats").setOrigin(0);
    const graphic = this.scene.add.graphics(config.frame.style);
    graphic.fillRect(config.frame.x, config.frame.y, config.frame.width, config.frame.height);
    graphic.strokeRect(config.frame.x, config.frame.y, config.frame.width, config.frame.height);
    const topText = this.scene.add.text(config.top.x, config.top.y, "YOU HAVE WON", config.textStyle);
    this.bottomText = this.scene.add.text(config.bottom.x, config.bottom.y, "IN FREE SPINS", config.textStyle);
    this.btnClose = new BasicImageButton(scene, config.closeButton.x, config.closeButton.y, "atlas_1", "close");
    this.btnClose.onButtonUp = () => this.closePopup();
    this.digits = new Digits(scene, config.digit.x, config.digit.y, "atlas_1", "orange_number/");
    this.digits.setScale(config.digit.scale);
    this.digits.displayWithString("49.20");
    this.add([bg, this.light, tittle, graphic, topText, this.bottomText, this.btnClose, this.digits]);
  }

  closePopup() {
    this.setVisible(false);
    this.emit(EVENT_NAME.CLOSE_FREE_GAME_POPUP);
  }

  showFreeTimes(num) {
    this.digits.displayWithString(num.toString());
    this.bottomText.setText("FREE SPINS");
    this.setVisible(true);
    this.scene.time.delayedCall(config.closeDelay, () => this.closePopup());
  }

  showTotalPayoff(str) {
    this.digits.displayWithString(str);
    this.bottomText.setText("IN FREE SPINS");
    this.setVisible(true);
    this.scene.time.delayedCall(config.closeDelay, () => this.closePopup());
  }
}
