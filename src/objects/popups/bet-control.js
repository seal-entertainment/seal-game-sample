import { EVENT_NAME } from "../../constants/events";
import { switchButton } from "../../utils";
import { BasicImageButton } from "../buttons/basic-image-button";

const config = {
  text: {
    tittle: {
      x: 133,
      y: 10,
      style: { fontSize: "24px", fontFamily: "Helvetica" },
    },
    bet: {
      x: 70,
      y: 71,
      style: {
        fontSize: "30px",
        fontFamily: "Helvetica",
        fixedWidth: 220,
        strokeThickness: 1,
        align: "center",
      },
    },
  },
  btn: {
    textureKey: "atlas_1",
    minus: {
      x: 0,
      y: 63,
      frameKey: "minusbet",
    },
    plus: {
      x: 310,
      y: 63,
      frameKey: "plusbet",
    },
  },
  graphic: {
    style: {
      fillStyle: { color: 0xffffff, alpha: 0.1 },
      lineStyle: { color: 0xffffff, alpha: 0.5 },
    },
    x: 70,
    y: 48,
    width: 220,
    height: 80,
    radius: 4,
  },
};

export class BetControl extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const tittle = this.scene.add.text(
      config.text.tittle.x,
      config.text.tittle.y,
      "Total Bet",
      config.text.tittle.style
    );
    this.btnMinus = new BasicImageButton(
      scene,
      config.btn.minus.x,
      config.btn.minus.y,
      config.btn.textureKey,
      config.btn.minus.frameKey
    ).setOrigin(0);
    this.btnPlus = new BasicImageButton(
      scene,
      config.btn.plus.x,
      config.btn.plus.y,
      config.btn.textureKey,
      config.btn.plus.frameKey
    ).setOrigin(0);
    const graphic = this.scene.add.graphics(config.graphic.style);
    graphic.fillRoundedRect(
      config.graphic.x,
      config.graphic.y,
      config.graphic.width,
      config.graphic.height,
      config.graphic.radius
    );
    graphic.strokeRoundedRect(
      config.graphic.x,
      config.graphic.y,
      config.graphic.width,
      config.graphic.height,
      config.graphic.radius
    );
    this.bet = this.scene.add.text(config.text.bet.x, config.text.bet.y, "YNG  1.00", config.text.bet.style);
    this.add([tittle, this.btnMinus, this.btnPlus, graphic, this.bet]);
    this.btnMinus.onButtonUp = () => this.scene.game.events.emit(EVENT_NAME.UPDATE_VALUE, -1);
    this.btnPlus.onButtonUp = () => this.scene.game.events.emit(EVENT_NAME.UPDATE_VALUE, 1);
    // this.btnMinus.onButtonDown = () => changeImageByState(this.btnMinus, "down");
    // this.btnPlus.onButtonDown = () => changeImageByState(this.btnPlus, "down");

    this.scene.game.events.on(EVENT_NAME.UPDATE_BET, (value) => this.updateBet(value));
  }

  updateBet(str) {
    this.bet.setText("YNG  " + str);
  }

  switchButton(on) {
    switchButton(this.btnMinus, on);
    switchButton(this.btnPlus, on);
  }
}
