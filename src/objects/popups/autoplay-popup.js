import Decimal from "decimal.js";
import { EVENT_NAME } from "../../constants/events";
import { BlueRoundedRectButton } from "../buttons/blue-rounded-rect-button";
import { BasicImageButton } from "../buttons/basic-image-button";
import { HorizontalSlider } from "../sliders/horizontal-slider";
import { BasicPopup } from "./basic-popup";
import { changeImageByState } from "../../utils";

const config = {
  width: 600,
  height: 600,
  text: {
    style: {
      fontSize: "24px",
      fontFamily: "Helvetica",
      strokeThickness: 1,
    },
    turbo: {
      x: 126,
      y: 179,
      size: 30,
    },
    slider: {
      x: 60,
      y: 306,
    },
    autoTimes: {
      x: 510,
      y: 380,
    },
  },
  btn: {
    turbo: {
      x: 403,
      y: 206,
      textureKey: "atlas_1",
      frameKey: "switch_off",
    },
    start: {
      x: 120,
      y: 454,
      textureKey: "bluebutton_up",
    },
  },
  line: {
    style: { color: 0xffffff, lineWidth: 2 },
    x1: 0,
    y1: 24,
    x2: 420,
    y2: 24,
    width: 420,
    height: 50,
    key: "trackline",
  },
  slider: {
    x: 270,
    y: 396,
    track: {
      texture: "trackline",
      frame: null,
    },
    thumb: {
      texture: "atlas_1",
      frame: "thumb",
    },
  },
};

export class AutoplayPopup extends BasicPopup {
  constructor(scene, x, y, outsideBtn, autoTimes = 50) {
    super(scene, x, y, config.width, config.height, outsideBtn);
    this.tittle.setText("AUTOPLAY SETTINGS");
    // -------Turbo Spin----------
    const turboText = this.scene.add.text(config.text.turbo.x, config.text.turbo.y, "Turbo Spin", config.text.style);
    turboText.setFontSize(config.text.turbo.size);
    this.btnTurbo = new BasicImageButton(
      scene,
      config.btn.turbo.x,
      config.btn.turbo.y,
      config.btn.turbo.textureKey,
      config.btn.turbo.frameKey
    );
    this.add([turboText, this.btnTurbo]);
    this.btnTurbo.onButtonUp = () => {
      this.btnTurbo.isClick = !this.btnTurbo.isClick;
      changeImageByState(this.btnTurbo, this.btnTurbo.isClick ? "on" : "off");
      this.emit(EVENT_NAME.TURBO_CHANGED, this.btnTurbo.isClick);
    };
    // ---------Number of auto spin--------------
    this.autoTimes = autoTimes;
    const autoSpinText = this.scene.add.text(
      config.text.slider.x,
      config.text.slider.y,
      "Number of auto spin",
      config.text.style
    );
    const trackMaker = this.scene.make.graphics();
    trackMaker.lineStyle(config.line.style.lineWidth, config.line.style.color);
    trackMaker.lineBetween(config.line.x1, config.line.y1, config.line.x2, config.line.y2);
    trackMaker.generateTexture(config.line.key, config.line.width, config.line.height);
    trackMaker.destroy();
    this.slider = new HorizontalSlider(
      scene,
      config.slider.x,
      config.slider.y,
      config.slider.track,
      config.slider.thumb
    );
    this.autoTimesText = this.scene.add.text(
      config.text.autoTimes.x,
      config.text.autoTimes.y,
      this.autoTimes,
      config.text.style
    );
    this.btnStartAuto = new BlueRoundedRectButton(
      scene,
      config.btn.start.x,
      config.btn.start.y,
      "Start auto spin [" + this.autoTimes + "]"
    );
    this.add([autoSpinText, this.slider, this.autoTimesText, this.btnStartAuto]);
    //----------Start auto spin [100]----------------------
    this.slider.on(EVENT_NAME.UPDATE_VALUE, (value) => {
      value = new Decimal(value).mul(100).toNumber();
      this.autoTimesText.setText(value);
      this.btnStartAuto.setText("Start auto spin [" + value + "]");
      this.autoTimes = value;
    });
    this.btnStartAuto.onButtonUp(() => {
      if (this.autoTimes > 0) this.emit(EVENT_NAME.AUTOPLAY, this.autoTimes);
      this.btnClose.onButtonUp();
    });
  }
}
