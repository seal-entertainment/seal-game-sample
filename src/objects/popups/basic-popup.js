import { EVENT_NAME } from "../../constants/events";
import { changeImageByState } from "../../utils";
import { BasicImageButton } from "../buttons/basic-image-button";

const config = {
  background: {
    style: {
      fillStyle: { color: 0x333333, alpha: 1 },
      lineStyle: { color: 0x787878, alpha: 1 },
    },
    radius: 20,
  },
  closeButton: {
    right: 40,
    y: 40,
    textureKey: "atlas_1",
    frameKey: "close",
  },
  tittle: {
    style: {
      font: "700 40px Helvetica",
      color: "#34a3d5",
      align: "center",
      lineSpacing: 6,
    },
    x: 0,
    y: 60,
  },
};

export class BasicPopup extends Phaser.GameObjects.Container {
  constructor(scene, x, y, width, height, outsideBtn = null) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.background = this.scene.add.graphics(config.background.style);
    this.background.setInteractive(new Phaser.Geom.Rectangle(0, 0, width, height), Phaser.Geom.Rectangle.Contains);
    this.background.fillRoundedRect(0, 0, width, height, config.background.radius);
    this.background.strokeRoundedRect(0, 0, width, height, config.background.radius);
    this.width = width;
    this.height = height;
    this.btnClose = new BasicImageButton(
      scene,
      width - config.closeButton.right,
      config.closeButton.y,
      config.closeButton.textureKey,
      config.closeButton.frameKey
    );
    this.tittle = this.scene.add.text(config.tittle.x, config.tittle.y, "TITTLE", config.tittle.style);
    this.tittle.setFixedSize(width, height);
    const zone = this.scene.add.zone(0, 0, width, height).setInteractive();
    this.add([zone, this.background, this.btnClose, this.tittle]);

    this.btnClose.onButtonUp = () => {
      this.setVisible(false);
      if (outsideBtn) {
        changeImageByState(outsideBtn, "up");
        outsideBtn.isClick = false;
      }
    };

    this.scene.events.on(EVENT_NAME.CLOSE_POPUP, () => this.btnClose.onButtonUp());
    this.setVisible(false);
  }

  updateSize(width, height) {
    this.background.clear();
    this.background.fillRoundedRect(0, 0, width, height, config.background.radius);
    this.background.strokeRoundedRect(0, 0, width, height, config.background.radius);
    this.width = width;
    this.height = height;
  }
}
