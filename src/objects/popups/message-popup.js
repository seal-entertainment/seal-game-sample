import { BasicPopup } from "./basic-popup";
import { BlueRoundedRectButton } from "../buttons/blue-rounded-rect-button";

const config = {
  width: 600,
  height: 360,
  messageText: {
    style: {
      fontSize: "30px",
      fontFamily: "Helvetica",
      strokeThickness: 1,
      align: "center",
      fixedWidth: 600,
      wordWrap: { width: 200 },
    },
    x: 0,
    y: 146,
    minHeight: 40,
  },
  blueButton: {
    left: 120,
    bottom: 140,
  },
};

export class MessagePopup extends BasicPopup {
  constructor(scene, x, y) {
    super(scene, x, y, config.width, config.height);
    this.tittle.setText("MESSAGE");

    this.message = this.scene.add.text(config.messageText.x, config.messageText.y, "Message", config.messageText.style);
    this.btnBlue = new BlueRoundedRectButton(
      scene,
      config.blueButton.left,
      this.height - config.blueButton.bottom,
      "CLOSE"
    );
    this.btnBlue.onButtonUp(() => this.setVisible(false));
    this.add([this.message, this.btnBlue]);
  }

  setMessage(str) {
    this.message.setText(str);
    if (this.message.height > config.messageText.minHeight) this.updateHeight();
  }

  updateHeight() {
    this.updateSize(this.width, config.height - config.messageText.minHeight + this.message.height);
    this.btnBlue.setY(this.height - config.blueButton.bottom);
  }
}
