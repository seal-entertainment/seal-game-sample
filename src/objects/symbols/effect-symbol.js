import { Digits } from "../digits";
import { BasicSymbol } from "./basic-symbol";

const config = {
  double: { x: 0, y: -10, stringSpacing: 0, digitSpacing: 0 },
  scatterIndex: 9,
  crashEffect: {
    texture: "atlas_3",
    key: "explosion",
    frameNames: [
      {
        texture: "atlas_3",
        prefix: "explosion/",
        suffix: "",
        start: 1,
        end: 16,
        zeroPad: 2,
      },
    ],
    repeat: 0,
    scale: 0.25,
  },
  scatter: {
    texture: "atlas_2",
    key: "scatter",
    frameNames: [
      {
        texture: "atlas_2",
        prefix: "scatter/",
        start: 1,
        end: 24,
        zeroPad: 2,
      },
    ],
    repeat: 0,
    scale: 0.475,
  },
  explosionDelay: [600, 1000],
  tween: {
    crash: {
      targets: [],
      ease: "Linear",
      props: {
        scaleX: {
          value: 0.6,
          yoyo: true,
          duration: 300,
        },
        scaleY: {
          value: 0.6,
          yoyo: true,
          duration: 300,
        },
        alpha: {
          value: 0,
          delay: 600,
          duration: 200,
        },
      },
    },
    double: {
      targets: [],
      ease: "Linear",
      props: {
        scaleX: 1.2,
        scaleY: 1.2,
      },
      duration: 300,
      yoyo: true,
    },
    flame: {
      targets: [],
      ease: "Linear",
      props: {
        alpha: {
          value: 1,
          yoyo: true,
          duration: 300,
          hold: 300,
        },
      },
    },
  },
};

export class SymbolWithExplosion extends BasicSymbol {
  constructor(scene, card) {
    super(scene, card);

    this.flame = this.scene.add.image(0, 0, "atlas_1", "symbol/flame");
    this.flame.setAlpha(0);
    this.add(this.flame);

    this.scatterRinging = this.setAnimation(config.scatter);
    this.scatterRinging.setScale(config.scatter.scale).setVisible(false);

    const crashEffect = this.setAnimation(config.crashEffect);
    crashEffect.setScale(config.crashEffect.scale);
  }

  createImage(card) {
    super.createImage(card);
    this.createDouble();
    this.double.setVisible(this.isDouble);
    if (this.isDouble) this.double.displayWithString(this.multiplying + "x");
  }

  createDouble() {
    this.double = new Digits(this.scene, config.double.x, config.double.y, "atlas_1", "double/");
    this.double.setCharacter(12, "x");
    this.double.setStringSpacing(config.double.stringSpacing);
    this.double.setDigitSpacing(config.double.digitSpacing);
    this.add(this.double);
  }

  changeCard(card) {
    super.changeCard(card);
    this.double.setVisible(this.isDouble);
    if (this.isDouble) this.double.displayWithString(this.multiplying + "x");
  }

  crash() {
    let flameConfig = config.tween.flame;
    flameConfig.targets = [this.flame];
    flameConfig.onComplete = (flameTween) => {
      this.scene.tweens.remove(flameTween);
    };
    this.scene.tweens.add(flameConfig);

    let crashConfig = config.tween.crash;
    crashConfig.targets = [this.cardImg];
    crashConfig.onComplete = (crashTween) => {
      this.scene.tweens.remove(crashTween);
    };
    this.scene.tweens.add(crashConfig);

    this.scene.time.delayedCall(config.explosionDelay[0], () => this.playAnim(config.crashEffect.key));
  }

  playScatter() {
    this.playAnim(config.scatter.key);
    this.scatterRinging.setVisible(true);
    this.scene.time.delayedCall(config.explosionDelay[1], () => {
      this.playAnim(config.crashEffect.key);
      this.scatterRinging.setVisible(false);
    });
  }

  playDouble() {
    let tweenConfig = config.tween.double;
    tweenConfig.targets = [this];
    tweenConfig.onComplete = (tween) => {
      this.scene.tweens.remove(tween);
      this.double.setVisible(false);
    };
    this.scene.tweens.add(tweenConfig);
  }
}
