import { SpriteSheetAnimation } from "../animation/sprite-sheet-animation";

const config = {
  scatterIndex: 9,
  scatterCard: 100,
  scale: 0.475,
  doubleLimit: [10, 20, 100, 500], // 乘倍符號的範圍 [2,3,4,5,6,8,] [10,12,15] [20,25,50] [100,250,500]
  textureKey: ["atlas_1"],
  frameKey: [
    "symbol/01",
    "symbol/02",
    "symbol/03",
    "symbol/04",
    "symbol/05",
    "symbol/06",
    "symbol/07",
    "symbol/08",
    "symbol/09",
    "symbol/10",
    "symbol/11",
    "symbol/12",
    "symbol/13",
    "symbol/14",
  ],
};

export class BasicSymbol extends Phaser.GameObjects.Container {
  constructor(scene, card) {
    super(scene);
    this.scene.add.existing(this);
    this.anims = [];
    this.card = card;
    this.multiplying = 1;
    this.createImage(card);
  }

  createImage(card) {
    const index = this.findCardImageIndex(card);
    if (config.textureKey.length === 1)
      this.cardImg = this.scene.add.image(0, 0, config.textureKey, config.frameKey[index]);
    else this.cardImg = this.scene.add.image(0, 0, config.textureKey[index]);
    this.cardImg.setScale(config.scale);
    this.add(this.cardImg);
  }

  // 後端卡號: 100 = scatter, 92 = 2倍, 910 = 10倍, 925 = 25倍......
  findCardImageIndex(card) {
    this.isDouble = false;
    this.isScatter = false;
    this.multiplying = 1;
    if (card < config.scatterIndex) return card;
    else if (card === config.scatterCard) {
      this.isScatter = true;
      return config.scatterIndex;
    } else {
      this.isDouble = true;
      this.multiplying = parseInt(card.toString().slice(1));
      for (let i = 0; i < config.doubleLimit.length; i++) {
        if (this.multiplying < config.doubleLimit[i]) return config.scatterIndex + 1 + i;
      }
    }
  }

  changeCard(card) {
    this.card = card;
    const index = this.findCardImageIndex(card);
    if (config.textureKey.length === 1) this.cardImg.setTexture(config.textureKey, config.frameKey[index]);
    else this.cardImg.setTexture(config.textureKey[index]);
  }

  setAnimation(animationConfig) {
    const anim = new SpriteSheetAnimation(this.scene, 0, 0, animationConfig);
    this.anims.push(anim);
    this.add(anim);
    return anim;
  }

  getAnim(key) {
    return this.anims.find((anim) => anim.key === key);
  }

  playAnim(key) {
    const anim = this.getAnim(key);
    if (anim) {
      anim.play(key);
    }
  }
}
