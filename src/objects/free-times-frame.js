import { Digits } from "./digits";

const config = {
  style: { fillStyle: { color: 0x212b27 }, lineStyle: { color: 0xffffff } },
  width: 180,
  height: 150,
  radius: 10,
  text: {
    x: 0,
    y: 13,
    style: {
      fontSize: "24px",
      fontFamily: "Helvetica",
      strokeThickness: 1,
      align: "center",
      fixedWidth: 180,
      wordWrap: { width: 150 },
      shadow: {
        color: "#432810",
        offsetY: 1,
        blur: 2,
        stroke: true,
      },
    },
  },
  digit: {
    x: 90,
    y: 67,
    scale: 1.5,
  },
};

export class FreeTimesFrame extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    const bg = this.scene.add.image(0, 0, "atlas_4", "freetimesbox").setOrigin(0);
    const text = this.scene.add.text(config.text.x, config.text.y, "FREE SPINS LEFT", config.text.style);
    this.freeTimes = new Digits(scene, config.digit.x, config.digit.y, "atlas_1", "orange_number/");
    this.freeTimes.setScale(config.digit.scale);
    this.add([bg, text, this.freeTimes]);
    this.setVisible(false);
    this.freeTimes.displayWithString("8");
  }

  updateFreeTimes(value) {
    this.setVisible(true);
    this.freeTimes.displayWithString(value.toString());
  }
}
