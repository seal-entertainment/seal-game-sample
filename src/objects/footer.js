import { formatNumStrToTwoDecimal } from "../utils";
import { currencyMap } from "../constants/currency";

const config = {
  textureKey: "atlas_1",
  icon: {
    balance: { x: 140, y: 18 },
    bet: { x: 490, y: 18 },
    scale: 0.9,
  },
  text: {
    style: {
      fontSize: "20px",
      fontFamily: "Helvetica",
    },
    balance: { x: 190, y: 24 },
    bet: { x: 540, y: 24 },
  },
  digits: {
    balance: { x: 280, y: 23 },
    bet: { x: 590, y: 23 },
    scale: 0.7,
    spaceSpacing: 10,
    style: {
      font: "700 20px Helvetica",
      align: "center",
    },
    startOffSetColor: "#FFDE00",
    endOffSetColor: "#FF8400",
    currencyNum: 72,
  },
};
export class Footer extends Phaser.GameObjects.Container {
  constructor(scene, x, y) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.balanceIcon = this.scene.add.image(
      config.icon.balance.x,
      config.icon.balance.y,
      config.textureKey,
      "walleticon"
    );
    this.balanceIcon.setOrigin(0).setScale(config.icon.scale);
    this.balanceName = this.scene.add.text(config.text.balance.x, config.text.balance.y, "CREDIT", config.text.style);
    this.defaultCurrency = config.digits.currencyNum;
    this.balance = this.scene.add.text(config.digits.balance.x, config.digits.balance.y, "0", config.digits.style);
    this.betIcon = this.scene.add.image(config.icon.bet.x, config.icon.bet.y, config.textureKey, "beticon");
    this.betIcon.setOrigin(0).setScale(config.icon.scale);
    this.betName = this.scene.add.text(config.text.bet.x, config.text.bet.y, "BET", config.text.style);
    this.bet = this.scene.add.text(config.digits.bet.x, config.digits.bet.y, "0.00", config.digits.style);
    this.add([this.balanceIcon, this.balanceName, this.betIcon, this.betName, this.balance, this.bet]);
  }

  updateBalance(balance, currencyNum = this.defaultCurrency) {
    const currencyBalance = `${currencyMap[currencyNum]} ${formatNumStrToTwoDecimal(balance)}`;
    this.balance.setText(currencyBalance);
    this.addLinearGradient(this.balance);
  }

  updateBet(bet, currencyNum = this.defaultCurrency) {
    const currencyBet = `${currencyMap[currencyNum]} ${formatNumStrToTwoDecimal(bet)}`;
    this.bet.setText(currencyBet);
    this.addLinearGradient(this.bet);
  }

  addLinearGradient(node) {
    const gradient = node.context.createLinearGradient(0, 0, 0, node.height);
    gradient.addColorStop(0, config.digits.startOffSetColor);
    gradient.addColorStop(1, config.digits.endOffSetColor);
    node.setFill(gradient);
  }

  resize() {}
}
