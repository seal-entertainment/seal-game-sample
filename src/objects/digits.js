import Phaser from "phaser";

export class Digits extends Phaser.GameObjects.Container {
  constructor(scene, x, y, textureName, digitsName) {
    super(scene, x, y);
    this.scene.add.existing(this);
    this.characterIndex = { 0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, ".": 10, ",": 11 };
    this.array = [];
    this.useComma = false;
    this.findArea = false;
    this.digitSpacing = -2;
    this.stringSpacing = -4;
    this.spaceSpacing = 20;
    this.unnumberIndex = 10;
    this.box = this.scene.add.container();
    this.textureName = textureName;
    this.digitsName = digitsName;
    this.add(this.box);
  }

  setDigitSpacing(px) {
    this.digitSpacing = px;
  }

  setStringSpacing(px) {
    this.stringSpacing = px;
  }

  setCharacter(index, character) {
    this.characterIndex[character] = index;
  }

  displayWithString(str, type = "center") {
    this.box.removeAll(true);
    this.box.width = 0;
    for (let i = 0; i < str.length; i++) {
      if (str.charAt(i) === " ") {
        this.addSpace();
        continue;
      }
      const index = this.characterIndex[str.charAt(i)];
      const spacing = index < this.unnumberIndex ? this.digitSpacing : this.stringSpacing;
      if (index >= this.unnumberIndex) this.box.width += this.stringSpacing;
      const digit = this.scene.add.image(this.box.width, 0, this.textureName, this.digitsName + index).setOrigin(0);
      this.box.height = digit.height;
      this.box.width += digit.width + spacing;
      this.box.add(digit);
    }
    this.align(type);
  }

  align(type) {
    if (type === "left") this.box.x = 0;
    else if (type === "right") this.box.x = -this.box.width;
    else this.box.x = -0.5 * this.box.width;
    if (this.findArea) this.showArea();
  }

  // 顯示 Digits box範圍，開發用
  showArea() {
    const frame = this.scene.add.graphics({ lineStyle: { color: 0xff0000 } });
    frame.strokeRect(this.box.x, this.box.y, this.box.width, this.box.height);
    this.add(frame);
  }

  addSpace() {
    this.box.width += this.spaceSpacing;
  }
}
