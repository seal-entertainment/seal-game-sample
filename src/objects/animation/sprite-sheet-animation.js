export class SpriteSheetAnimation extends Phaser.GameObjects.Sprite {
  constructor(scene, x, y, config) {
    super(scene, x, y, config.texture);
    this.scene.add.existing(this);
    this.key = config.key;
    this.animationConfig = config;
    if (config.frameNames) {
      this.animationConfig.frames = this.generateFrames(scene, this.generateFrameNames, config.frameNames);
    } else if (config.frameNumbers) {
      this.animationConfig.frames = this.generateFrames(scene, this.generateFrameNumbers, config.frameNumbers);
    }
    this.scene.anims.create(this.animationConfig);
  }

  generateFrames(scene, fun, config) {
    if (config.length) {
      let frames = [];
      for (let i = 0; i < config.length; i++) {
        frames.push(...fun(scene, config[i]));
      }
      return frames;
    }
  }

  generateFrameNames(scene, config) {
    return scene.anims.generateFrameNames(config.texture, config);
  }

  generateFrameNumbers(scene, config) {
    return scene.anims.generateFrameNumbers(config.texture, config);
  }
}
