import Phaser from "phaser";
import CoveringScene from "./scenes/covering-scene";
import LoadingScene from "./scenes/loading-scene";
import MainScene from "./scenes/main-scene";

const config = {
  type: Phaser.AUTO,
  parent: "app",
  scale: {
    mode: Phaser.Scale.HEIGHT_CONTROLS_WIDTH,
    width: 1280,
    height: 1280,
  },
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 200 },
    },
  },
  scene: [LoadingScene, MainScene, CoveringScene],
};

const game = new Phaser.Game(config);
