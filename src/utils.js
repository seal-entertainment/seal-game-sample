// 將 playerInfo 存到 sessionStorage
export const setAuthPlayerInfo = (playerInfo) => {
  sessionStorage.setItem("playerInfo", JSON.stringify(playerInfo));
};
// 從 sessionStorage 讀取 playerInfo
export const getAuthPlayerInfo = () => {
  return JSON.parse(sessionStorage.getItem("playerInfo"));
};
// 清除 sessionStorage playerInfo
export const removeAuthPlayerInfo = () => {
  return sessionStorage.removeItem("playerInfo");
};

// 依據按鈕狀態，更換圖片
export const changeImageByState = (btn, state) => {
  if (btn.frame.name === "__BASE") {
    const frameName = btn.texture.key.split("_")[0];
    btn.setTexture(frameName + "_" + state);
  } else {
    const frameName = btn.frame.name.split("_")[0];
    btn.setTexture(btn.texture.key, frameName + "_" + state);
  }
};

export const addLinearGradient = (phaserText, style) => {
  const gradient = phaserText.context.createLinearGradient(0, 0, 0, phaserText.height);
  for (let i = 0; i < style.length; i++) {
    gradient.addColorStop(style[i].stop, style[i].color);
  }
  phaserText.setFill(gradient);
};

// 鎖住/解鎖按鈕
export const switchButton = (btn, on) => {
  if (on) btn.setInteractive();
  else btn.disableInteractive();
};

// 三位數字串加 ',' 小數點取至2位數補 0
export const formatNumStrToTwoDecimal = (numStr) => {
  const num = parseFloat(numStr);
  const [intPart, decimalPart] = num.toFixed(2).split(".");
  const formattedIntPart = intPart.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  return `${formattedIntPart}.${decimalPart || "00"}`;
};
