# Seal-Game-8901

### Description

This project is using Phaser 3 + JavaScript + Vite by Seal Entertainment.

## Project Setup and Commands

git clone https://git.owdev.net/seal/seal-game-8901.git ([gitlab])

yarn : Install project dependencies.

yarn dev : Start the vite dev server for development.

yarn build : Build the app for production.

yarn preview : Preview the production build locally.

## Directory Structure

```
├─ public/                      # 靜態資源
    ├─ images/                  # 圖片
    └─ sounds/                  # 音樂音效
├─ src/                         # 根目錄
    ├─ objects/                 # 遊戲物件
    ├─ scenes/                  # 遊戲場景
    ├─ constants/               # 共用參數
    └─ index.js                 # 初始化遊戲
├─ .gitignore
├─  favicon.png
├─  index.html                  # 專案進入點
├─  node_modules
├─  package.json
├─  yarn.lock
├─  vite.config.js
└─  README.md

```
